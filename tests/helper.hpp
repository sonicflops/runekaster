#ifndef HELPER_HPP
#define HELPER_HPP

#include <iostream>
#include "catch.hpp"
#include "audio_common.hpp"

sfsw::audio::f32sample_uptr generate_stereo(sfsw::audio::frame_count num_frames);
sfsw::audio::f32sample_uptr generate_mono(sfsw::audio::frame_count num_frames);
sfsw::audio::f32sample_uptr generate_zero_fill(sfsw::audio::frame_count num_frames, sfsw::audio::channel_count num_channels = 2);
void assert_stereo_window(sfsw::audio::f32sample_wrk left,
                            sfsw::audio::f32sample_wrk right,
                            sfsw::audio::f32sample_wrk data,
                            sfsw::audio::frame_count data_from_frame,
                            sfsw::audio::frame_count total_frames);
void assert_mono_window(sfsw::audio::f32sample_wrk left,
                            sfsw::audio::f32sample_wrk right,
                            sfsw::audio::f32sample_wrk data,
                            sfsw::audio::frame_count data_from_frame,
                            sfsw::audio::frame_count total_frames);

void debug_print_stereo_frames(sfsw::audio::f32sample_wrk left,
                        sfsw::audio::f32sample_wrk right,
                        sfsw::audio::frame_count total_frames);

#endif /* HELPER_HPP */

