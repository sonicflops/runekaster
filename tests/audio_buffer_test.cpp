#include "catch.hpp"
#include "helper.hpp"
#include "audio_buffer.hpp"
#include "audio_pass_basic.hpp"
using namespace sfsw::audio;

TEST_CASE( "audio_buffer sanity", "[audio][audio_buffer]" ) {
  frame_count period_size = 4;
  frame_count buffer_size = period_size;
  channel_count num_channels = 2;
  sample_count num_samples = buffer_size * num_channels;
  
  buffer b(period_size, num_channels);
  REQUIRE_FALSE( b.data_ready() );
  REQUIRE( b.state() == buffer::empty );
  REQUIRE( b.num_channels() == num_channels );
  REQUIRE( b.num_frames() == period_size );
  REQUIRE( b.num_samples() == num_samples );

}

TEST_CASE( "audio_buffer.copy_from stereo", "[audio][audio_buffer]" ) {
  frame_count period_size = 4;
  frame_count buffer_size = period_size;
  channel_count num_channels = 2;
  sample_count num_samples = buffer_size * num_channels;
  
  auto source = generate_stereo(period_size);
  buffer b(period_size, num_channels);
  
  
  b.copy_from(source.get(), period_size);
  
  
  REQUIRE( b.data_ready() );
  REQUIRE( b.state() == buffer::ready );
  
  for(auto  i = 0u; i < num_samples ; i++) {
    REQUIRE(b.data()[i] == source[i]);
  }
}

TEST_CASE( "audio_buffer.copy_from mono", "[audio][audio_buffer]" ) {
  frame_count period_size = 4;
  frame_count buffer_size = period_size;
  channel_count num_channels = 1;
  sample_count num_samples = buffer_size * num_channels;
  
  auto source = generate_mono(period_size);
  buffer b(period_size, num_channels);
  
  
  b.copy_from(source.get(), period_size);
  
  
  REQUIRE( b.data_ready() );
  REQUIRE( b.state() == buffer::ready );
  
  for(auto  i = 0u; i < num_samples ; i++) {
    REQUIRE(b.data()[i] == source[i]);
  }
}

TEST_CASE( "audio_buffer.copy_window stereo full", "[audio][audio_buffer]" ) {
  frame_count period_size = 4;
  frame_count buffer_size = period_size;
  channel_count num_channels = 2;
  sample_count num_samples = buffer_size * num_channels;
  
  auto source = generate_stereo(period_size);

  buffer b( buffer_size, num_channels);
  signal s( period_size );
  
  b.copy_from(source.get(), period_size);
  b.copy_window(s);
  
  auto left = s.channel(stereo_channel::left);
  auto right = s.channel(stereo_channel::right);
  assert_stereo_window(left,right,source.get(), 0, period_size);
}

TEST_CASE( "audio_buffer.seek", "[audio][audio_buffer]" ) {
  frame_count period_size = 8;
  frame_count buffer_size = period_size;
  channel_count num_channels = 2;
  sample_count num_samples = buffer_size * num_channels;
  buffer b( buffer_size, num_channels);
  b.notify_data_ready();
  
  SECTION("linear in scope") {
    b.seek(6);
    REQUIRE( b.position() == 6 );
  }
  
  SECTION("linear out of scope") {
    b.seek(10);
    REQUIRE( b.position() == buffer_size - 1 );
    REQUIRE( b.state() == buffer::finished );
  }
  
  SECTION("linear out of scope two move") {
    b.seek(6);
    b.seek(6);
    REQUIRE( b.position() == buffer_size - 1 );
    REQUIRE( b.state() == buffer::finished );
  }
  
  SECTION("looped in scope") {
    b.set_behaviour(buffer::looped_iterator);
    b.seek(6);
    REQUIRE( b.position() == 6 );
  }
  
  SECTION("looped out of scope small") {
    b.set_behaviour(buffer::looped_iterator);
    b.seek(10);
    REQUIRE( b.position() == 2 );
  }
  
  SECTION("looped out of scope large") {
    b.set_behaviour(buffer::looped_iterator);
    b.seek(20);
    REQUIRE( b.position() == 4 );
  }
}

TEST_CASE( "audio_buffer.rewind", "[audio][audio_buffer]" ) {
  frame_count period_size = 8;
  frame_count buffer_size = period_size;
  channel_count num_channels = 2;
  sample_count num_samples = buffer_size * num_channels;
  buffer b( buffer_size, num_channels);
  b.notify_data_ready();
  b.seek(6);
  
  SECTION("linear in scope") {
    b.rewind(3);
    REQUIRE( b.position() == 3 );
  }
  
  SECTION("linear out of scope") {
    b.rewind(10);
    REQUIRE( b.position() == 0 );
    REQUIRE( b.state() == buffer::ready );
  }
  
  SECTION("linear out of scope two move") {
    b.rewind(3);
    b.rewind(4);
    REQUIRE( b.position() == 0 );
    REQUIRE( b.state() == buffer::ready );
  }
  
  SECTION("looped in scope") {
    b.set_behaviour(buffer::looped_iterator);
    b.rewind(3);
    REQUIRE( b.position() == 3 );
  }
  
  SECTION("looped out of scope small") {
    b.set_behaviour(buffer::looped_iterator);
    b.rewind(9);
    REQUIRE( b.position() == 5 );
  }
  
  SECTION("looped out of scope large") {
    b.set_behaviour(buffer::looped_iterator);
    b.rewind(18);
    REQUIRE( b.position() == 4 );
  }
}

TEST_CASE( "audio_buffer.copy_window stereo window linear", "[audio][audio_buffer]" ) {
  frame_count period_size = 4;
  frame_count buffer_size = period_size * 3;
  channel_count num_channels = 2;
  sample_count num_samples = buffer_size * num_channels;
  
  auto source = generate_stereo(buffer_size);

  buffer b( buffer_size, num_channels);
  signal s( period_size );
  
  b.copy_from(source.get(), buffer_size);

  auto left = s.channel(stereo_channel::left);
  auto right = s.channel(stereo_channel::right);
  
  SECTION( "single window" ) {
    b.copy_window(s);
    assert_stereo_window(left,right,source.get(), 0, period_size);
    REQUIRE(b.state() == buffer::active);
  }
  
  SECTION( "all windows" ) {
    for(auto win = 0u; win < 3; win++) {
      b.copy_window(s);
      auto data_from = period_size * win;
      assert_stereo_window(left,right,source.get(), data_from, period_size);
      s.reset();
    }
    
    REQUIRE(b.state() == buffer::ready);
  }
  
  SECTION( "partial window zero fill" ) {
    signal ns(buffer_size + period_size);
    b.copy_window(ns);
    auto zeros = generate_zero_fill(period_size,2);
    left = ns.channel(stereo_channel::left);
    right = ns.channel(stereo_channel::right);
    
    // Check the partial filled window from buffer
    assert_stereo_window(left,right,source.get(), 0, buffer_size);
    
    // Check the zero filled window from buffer
    assert_stereo_window(left + buffer_size, right+buffer_size, zeros.get(), 0, period_size);
    
  }
}

TEST_CASE( "audio_buffer.copy_window mono window linear", "[audio][audio_buffer]" ) {
  frame_count period_size = 4;
  frame_count buffer_size = period_size * 3;
  channel_count num_channels = 1;
  sample_count num_samples = buffer_size * num_channels;
  
  auto source = generate_mono(buffer_size);

  buffer b( buffer_size, num_channels);
  signal s( period_size );
  
  b.copy_from(source.get(), buffer_size);

  auto left = s.channel(stereo_channel::left);
  auto right = s.channel(stereo_channel::right);
  
  SECTION( "single window" ) {
    b.copy_window(s);
    assert_mono_window(left,right,source.get(), 0, period_size);
  }
  
  SECTION( "all windows" ) {
    for(auto win = 0u; win < 3; win++) {
      b.copy_window(s);
      auto data_from = period_size * win;
      assert_mono_window(left,right,source.get(), data_from, period_size);
      s.reset();
    }
  }
  
  SECTION( "partial window zero fill" ) {
    signal ns(buffer_size + period_size);
    b.copy_window(ns);
    auto zeros = generate_zero_fill(period_size,1);
    left = ns.channel(stereo_channel::left);
    right = ns.channel(stereo_channel::right);
    
    // Check the partial filled window from buffer
    assert_mono_window(left,right,source.get(), 0, buffer_size);
    
    // Check the zero filled window from buffer
    assert_mono_window(left + buffer_size, right+buffer_size, zeros.get(), 0, period_size);
    
  }
}


TEST_CASE( "audio_buffer.copy_window stereo window looped", "[audio][audio_buffer]" ) {
  frame_count period_size = 4;
  frame_count buffer_size = period_size * 3;
  channel_count num_channels = 2;
  sample_count num_samples = buffer_size * num_channels;
  
  auto source = generate_stereo(buffer_size);

  buffer b( buffer_size, num_channels);
  b.set_behaviour(buffer::looped_iterator);
  
  signal s( period_size );
  
  b.copy_from(source.get(), buffer_size);

  auto left = s.channel(stereo_channel::left);
  auto right = s.channel(stereo_channel::right);
  
  SECTION( "single window" ) {
    b.copy_window(s);
    assert_stereo_window(left,right,source.get(), 0, period_size);
  }
  
  SECTION( "all windows and one more" ) {
    for(auto win = 0u; win < 3; win++) {
      b.copy_window(s);
      auto data_from = period_size * win;
      assert_stereo_window(left,right,source.get(), data_from, period_size);
      s.reset();
    }
    b.copy_window(s);
    assert_stereo_window(left,right,source.get(), 0, period_size);
  }
  
  SECTION( "partial window loop fill" ) {
    signal ns(buffer_size + period_size);
    b.copy_window(ns);
    left = ns.channel(stereo_channel::left);
    right = ns.channel(stereo_channel::right);
    
    // Check the partial filled window from buffer
    assert_stereo_window(left,right,source.get(), 0, buffer_size);
    
    // Check the looped filled window from buffer
    assert_stereo_window(left + buffer_size, right+buffer_size, source.get(), 0, period_size);
    
  }
}

TEST_CASE( "audio_buffer.copy_window mono window looped", "[audio][audio_buffer]" ) {
  frame_count period_size = 4;
  frame_count buffer_size = period_size * 3;
  channel_count num_channels = 1;
  sample_count num_samples = buffer_size * num_channels;
  
  auto source = generate_mono(buffer_size);

  buffer b( buffer_size, num_channels);
  b.set_behaviour(buffer::looped_iterator);
  
  signal s( period_size );
  
  b.copy_from(source.get(), buffer_size);

  auto left = s.channel(stereo_channel::left);
  auto right = s.channel(stereo_channel::right);
  
  SECTION( "single window" ) {
    b.copy_window(s);
    assert_mono_window(left,right,source.get(), 0, period_size);
  }
  
  SECTION( "all windows and one more" ) {
    for(auto win = 0u; win < 3; win++) {
      b.copy_window(s);
      auto data_from = period_size * win;
      assert_mono_window(left,right,source.get(), data_from, period_size);
      s.reset();
    }
    b.copy_window(s);
    assert_mono_window(left,right,source.get(), 0, period_size);
  }
  
  SECTION( "partial window loop fill" ) {
    signal ns(buffer_size + period_size);
    b.copy_window(ns);
    left = ns.channel(stereo_channel::left);
    right = ns.channel(stereo_channel::right);
    
    // Check the partial filled window from buffer
    assert_mono_window(left,right,source.get(), 0, buffer_size);
    
    // Check the looped filled window from buffer
    assert_mono_window(left + buffer_size, right+buffer_size, source.get(), 0, period_size);
    
  }
}

TEST_CASE( "audio_buffer visitor stereo window linear", "[audio][audio_buffer]" ) {
  frame_count period_size = 4;
  frame_count buffer_size = period_size * 3;
  channel_count num_channels = 2;
  sample_count num_samples = buffer_size * num_channels;
  
  auto source = generate_stereo(buffer_size);

  buffer b( buffer_size, num_channels);
  signal s( period_size );
  
  b.copy_from(source.get(), buffer_size);

  auto left = s.channel(stereo_channel::left);
  auto right = s.channel(stereo_channel::right);
  
  s.pass(b); // Perform fill operation as visitor
  assert_stereo_window(left,right,source.get(), 0, period_size);
 
}

TEST_CASE( "audio_buffer prefill stereo window linear", "[audio][audio_buffer]" ) {
  frame_count period_size = 8;
  frame_count buffer_size = period_size;
  channel_count num_channels = 2;
  sample_count num_samples = buffer_size * num_channels;

  
  auto source = generate_stereo(buffer_size);
  auto zeros = generate_zero_fill(3, num_channels);

  buffer b( buffer_size, num_channels);
  
  signal s( period_size );
  
  // Fill buffer
  b.copy_from(source.get(), buffer_size);
  
  
  s.pass( pass::zero_fill(3) ); // fill 3 frames of the signal with zero
  s.pass(b); // fill from buffer

  auto left = s.channel(stereo_channel::left);
  auto right = s.channel(stereo_channel::right);
  
  
  REQUIRE(b.position() == period_size - 3);
  assert_stereo_window(left, right, zeros.get(), 0, 3);
  assert_stereo_window(left + 3, right+3, source.get(), 0, period_size - 3);
}

TEST_CASE( "audio_buffer zero fill empty buffer", "[audio][audio_buffer]" ) {
  frame_count period_size = 4;
  frame_count buffer_size = 0;
  channel_count num_channels = 2;
  sample_count num_samples = buffer_size * num_channels;

  
  auto zeros = generate_zero_fill(3, num_channels);

  buffer b( buffer_size, num_channels); // Empty, zero frame buffer
  signal s( period_size );
   
  s.pass(b); // fill from buffer (with zeros)

  auto left = s.channel(stereo_channel::left);
  auto right = s.channel(stereo_channel::right);
  


  assert_stereo_window(left, right, zeros.get(), 0, period_size);
}