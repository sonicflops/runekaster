#include "catch.hpp"
#include "audio_loader.hpp"

using namespace sfsw::audio;

void load_check() {
  auto b = load_buffer("442Hz-1s.flac");
  REQUIRE( b->data_ready() );
}

TEST_CASE( "load_buffer valid", "[audio][loader]" ) {
  
  REQUIRE_NOTHROW( load_check() );
  
}

TEST_CASE( "load_buffer file-not-found", "[audio][loader]" ) {
  REQUIRE_THROWS( load_buffer("invalid.flac") );
}