#include "catch.hpp"
#include "midi_event.hpp"
using namespace sfsw;

static int cb_trigger[] = {0,};

TEST_CASE( "midi_event callback", "[midi][event]" ) {
  midi::event_bus bus;
  bus.register_handler(midi::status_code::note_on, 0x41, [](const midi::message m){
    REQUIRE( m.status() == midi::status_code::note_on );
    REQUIRE( m.key() == 0x41 );
    cb_trigger[0]++;
  });
  
  SECTION("valid route") {
    std::uint8_t raw[] = {0x90, 0x41, 0xff};
    bus.handle(midi::message(raw));
    REQUIRE( cb_trigger[0] == 1 );
  }
  
  SECTION("invalid route") {
    std::uint8_t raw[] = {0x90, 0x42, 0xff};
    bus.handle(midi::message(raw));
    REQUIRE( cb_trigger[0] == 1 );
  }
}