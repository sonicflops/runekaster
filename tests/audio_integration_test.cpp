#include "catch.hpp"
#include "helper.hpp"
#include "workspace.hpp"

using namespace sfsw;
TEST_CASE( "multi layer syncing", "[audio][integration]" ) {
  sfsw::workspace ws(
    audio::stream_info(audio::frame_count(4),audio::channel_count(2))
  );
  
  ws.system_jack_client().open(nullptr,nullptr);
  
  
  ws.add_layer("subject");
  ws.add_layer("observer");
  auto& subject = ws.layer_from_label("subject");
  auto& observer = ws.layer_from_label("observer");
  
  subject.set_bahaviour(subject.free_loop);
  observer.set_bahaviour(observer.sync_loop);
    
  auto source_s = generate_stereo(audio::frame_count(8));
  auto source_o = generate_stereo(audio::frame_count(8));
  subject.load_audio(source_s.get(),audio::frame_count(8),audio::channel_count(2));
  observer.load_audio(source_o.get(),audio::frame_count(8),audio::channel_count(2));
  
  
  SECTION("No Sync") {
    REQUIRE(observer.state() == observer.ready);
    REQUIRE(subject.state() == subject.ready);
    subject.controller_start();
    REQUIRE(subject.state() == subject.active);
    for(auto i = 0u; i < 3; i++) {
      for(auto& layer : ws.system_active_locus().endpoints()) {
        layer.run_pipeline();
        REQUIRE(subject.state() == observer.active);
        REQUIRE(observer.state() == observer.ready);
      }
    }
    REQUIRE(subject.state() == observer.active);
    REQUIRE(observer.state() == observer.ready);
  }
  
  SECTION("With sync no cued observer") {

    subject.subscribe(
      subject.event_sync_start,
      delegate_0(observer, &audio::layer::on_sync_start)
    );

    REQUIRE(observer.state() == observer.ready);
    REQUIRE(subject.state() == subject.ready);

    subject.controller_start();
    REQUIRE(subject.state() == subject.active);

    for(auto i = 0u; i < 3; i++) {
      for(auto& layer : ws.system_active_locus().endpoints()) {
        layer.run_pipeline();
        REQUIRE(subject.state() == observer.active);
        REQUIRE(observer.state() == observer.ready);
      }
    }
  }
  
  SECTION("With Sync and cued observer") {
    
    subject.subscribe(
      subject.event_sync_start,
      delegate_0(observer, &audio::layer::on_sync_start)
    );
    

    subject.controller_start();
    observer.controller_start();
    REQUIRE(subject.state() == subject.active);
    REQUIRE(observer.state() == observer.cued);
    
    // Run through to the end of a buffer (set to 2 periods)
    // which is enough to reset the buffer but not trigger a
    // cued layer
    for(auto i = 0u; i < 2; i++) {
      for(auto& layer : ws.system_active_locus().endpoints()) {
        layer.run_pipeline();
        REQUIRE(subject.state() == observer.active);
        REQUIRE(observer.state() == observer.cued);
      }
    }
    
    // Loop round to beginning and trigger observer
    for(auto& layer : ws.system_active_locus().endpoints()) {
      layer.run_pipeline();
    }
    REQUIRE(subject.state() == observer.active);
    REQUIRE(observer.state() == observer.active);
    
    // Read to end and cue observer again (since they're both the same length)
    for(auto& layer : ws.system_active_locus().endpoints()) {
      layer.run_pipeline();
    }
    REQUIRE(subject.state() == observer.active);
    REQUIRE(observer.state() == observer.cued);
    
    // Loop round to beginning again and trigger observer
    for(auto& layer : ws.system_active_locus().endpoints()) {
      layer.run_pipeline();
    }
    REQUIRE(subject.state() == observer.active);
    REQUIRE(observer.state() == observer.active);
  }
}
