#include "catch.hpp"
#include "audio_layer.hpp"
#include "helper.hpp"
using namespace sfsw;

static unsigned int cb_trigger[] = {0, 0, 0};

void reset_trigger() {
  cb_trigger[0] = 0;
  cb_trigger[1] = 0;
  cb_trigger[2] = 0;
}

class layer_observer {
public:
  layer_observer()
    : _uid(sfsw::uid::generate())
  { }
    
  void on_sync_start() {
    cb_trigger[0] = 101;
  }
  
  void on_sync_bar(audio::frame_count frames) {
    cb_trigger[1] = frames;
  }
  
  sfsw::uid::unique_id uid(){ return _uid; }
  
private:
  sfsw::uid::unique_id _uid;
  
};

class triggered_pass : public audio::pass::signal_operation {
public:
  triggered_pass(unsigned int index) 
    : _index(index)
  { }
  
  void operator()(audio::signal& signal) override {
    cb_trigger[_index]++;
  }
  
  std::string name() override {
    return "test.trigger_pass";
  }

private:
  unsigned int _index;
};
TEST_CASE( "audio_layer construction", "[audio][layer]" ) {
  audio::layer layer("test_layer");
  REQUIRE(layer.label() == "test_layer");
}

TEST_CASE( "audio_layer start event callback", "[audio][layer]" ) {
  reset_trigger();
  audio::layer layer("test_layer");
  layer_observer observer;
  SECTION("invalid") {
    REQUIRE_NOTHROW(layer.event_sync_start.notify());
    REQUIRE(cb_trigger[0] == 0);
    REQUIRE(cb_trigger[1] == 0);
  }
  SECTION("valid") {
    layer.subscribe(layer.event_sync_start, delegate_0(observer, &layer_observer::on_sync_start));
    layer.event_sync_start.notify();
    REQUIRE(cb_trigger[0] == 101);
    REQUIRE(cb_trigger[1] == 0);
  }
}

TEST_CASE( "audio_layer bar event callback", "[audio][layer]" ) {
  reset_trigger();
  audio::layer layer("test_layer");
  layer_observer observer;
  SECTION("invalid") {
    REQUIRE_NOTHROW(layer.event_sync_bar.notify(5000));
    REQUIRE(cb_trigger[0] == 0);
    REQUIRE(cb_trigger[1] == 0);
  }
  SECTION("valid") {
    layer.subscribe(layer.event_sync_bar, delegate_1(observer, &layer_observer::on_sync_bar));
    layer.event_sync_bar.notify(5000);
    REQUIRE(cb_trigger[0] == 0);
    REQUIRE(cb_trigger[1] == 5000);
  }
  SECTION("unsubscribe") {
    layer.subscribe(layer.event_sync_bar, delegate_1(observer, &layer_observer::on_sync_bar));
    layer.unsubscribe(layer.event_sync_bar, observer);
    
    layer.subscribe(layer.event_sync_start, delegate_0(observer, &layer_observer::on_sync_start));
    layer.unsubscribe(layer.event_sync_start, observer);
    
    layer.event_sync_start.notify();
    layer.event_sync_bar.notify(5000);
    
    REQUIRE_FALSE(cb_trigger[0] == 101);
    REQUIRE_FALSE(cb_trigger[1] == 5000);
  }
}

TEST_CASE( "audio_layer behaviour changes behaviour::free_linear", "[audio][layer]" ) {
  audio::layer layer("test_layer");
  layer.set_bahaviour(audio::layer::free_linear);
  
  SECTION("empty") {
    REQUIRE(layer.state() == audio::layer::runtime_state::empty);
  }
  
  SECTION("ready") {
    auto source = generate_stereo(audio::frame_count(8));
    layer.load_audio(source.get(), 8, 2);
    REQUIRE(layer.state() == audio::layer::runtime_state::ready);
  }
  
  SECTION("active") {
    auto source = generate_stereo(audio::frame_count(8));
    layer.load_audio(source.get(), 8, 2);
    layer.controller_start();
    REQUIRE(layer.state() == audio::layer::runtime_state::active);
  }
}

TEST_CASE( "audio_layer behaviour changes behaviour::free_loop", "[audio][layer]" ) {
  audio::layer layer("test_layer");
  layer.set_bahaviour(audio::layer::free_loop);
  
  SECTION("empty") {
    REQUIRE(layer.state() == audio::layer::runtime_state::empty);
  }
  
  SECTION("ready") {
    auto source = generate_stereo(audio::frame_count(8));
    layer.load_audio(source.get(), 8, 2);
    REQUIRE(layer.state() == audio::layer::runtime_state::ready);
  }
  
  SECTION("active") {
    auto source = generate_stereo(audio::frame_count(8));
    layer.load_audio(source.get(), 8, 2);
    layer.controller_start();
    REQUIRE(layer.state() == audio::layer::runtime_state::active);
  }
  
  SECTION("active") {
    auto source = generate_stereo(audio::frame_count(8));
    layer.load_audio(source.get(), 8, 2);
    layer.controller_start();
    layer.on_sync_start();
    REQUIRE(layer.state() == audio::layer::runtime_state::active);
  }
}

TEST_CASE( "audio_layer behaviour changes behaviour::sync_linear", "[audio][layer]" ) {
  audio::layer layer("test_layer");
  layer.set_bahaviour(audio::layer::sync_linear);
  
  SECTION("empty") {
    REQUIRE(layer.state() == audio::layer::runtime_state::empty);
  }
  
  SECTION("ready") {
    auto source = generate_stereo(audio::frame_count(8));
    layer.load_audio(source.get(), 8, 2);
    REQUIRE(layer.state() == audio::layer::runtime_state::ready);
  }
  
  SECTION("cued") {
    auto source = generate_stereo(audio::frame_count(8));
    layer.load_audio(source.get(), 8, 2);
    layer.controller_start();
    REQUIRE(layer.state() == audio::layer::runtime_state::cued);
  }
  
  SECTION("active") {
    auto source = generate_stereo(audio::frame_count(8));
    layer.load_audio(source.get(), 8, 2);
    layer.controller_start();
    layer.on_sync_start();
    REQUIRE(layer.state() == audio::layer::runtime_state::active);
  }
}

TEST_CASE( "audio_layer behaviour changes behaviour::sync_loop", "[audio][layer]" ) {
  audio::layer layer("test_layer");
  layer.set_bahaviour(audio::layer::sync_loop);
  
  SECTION("empty") {
    REQUIRE(layer.state() == audio::layer::runtime_state::empty);
  }
  
  SECTION("ready") {
    auto source = generate_stereo(audio::frame_count(8));
    layer.load_audio(source.get(), 8, 2);
    REQUIRE(layer.state() == audio::layer::runtime_state::ready);
  }
  
  SECTION("cued") {
    auto source = generate_stereo(audio::frame_count(8));
    layer.load_audio(source.get(), 8, 2);
    layer.controller_start();
    REQUIRE(layer.state() == audio::layer::runtime_state::cued);
  }
  
  SECTION("active") {
    auto source = generate_stereo(audio::frame_count(8));
    layer.load_audio(source.get(), 8, 2);
    layer.controller_start();
    layer.on_sync_start();
    REQUIRE(layer.state() == audio::layer::runtime_state::active);
  }
}


TEST_CASE( "audio_layer.run_pipeline behaviour::free_linear", "[audio][layer]" ) {
  reset_trigger();
  audio::layer layer("test_layer", audio::frame_count(4));
  layer.set_bahaviour(audio::layer::free_linear);
  
  layer.add_pass(triggered_pass(2));
  
  SECTION("empty") {
    REQUIRE_FALSE(layer.run_pipeline());
    REQUIRE(cb_trigger[2] == 0);
  }
  
  SECTION("ready") {
    auto source = generate_stereo(audio::frame_count(8));
    layer.load_audio(source.get(), 8, 2);
       
    REQUIRE_FALSE(layer.run_pipeline());
    REQUIRE(cb_trigger[2] == 0);
  }
  
  SECTION("cycle single window buffer") {
    auto source = generate_stereo(4);
    layer.load_audio(source.get(), 4, 2);
    layer.controller_start();
    
    REQUIRE(layer.state() == audio::layer::active);
    REQUIRE(layer.run_pipeline());
    REQUIRE(cb_trigger[2] == 1);
    REQUIRE(layer.state() == audio::layer::ready);
  }
  
  SECTION("cycle 2 window buffer") {
    auto source = generate_stereo(audio::frame_count(8));
    layer.load_audio(source.get(), 8, 2);
    layer.controller_start();
    
    REQUIRE(layer.state() == audio::layer::active);
    auto count = 0;
    while(layer.state() == audio::layer::active) {
      REQUIRE(layer.run_pipeline());
      auto ep = layer.endpoint_reference();
      assert_stereo_window(
          ep.channel(audio::stereo_channel::left),
          ep.channel(audio::stereo_channel::right),
          source.get(),
          4 * count,
          4
      );
      count++;
    }
    REQUIRE(layer.state() == audio::layer::ready);
    REQUIRE(cb_trigger[2] == 2); // two windows of pipelines
    
  }
}

TEST_CASE( "audio_layer.run_pipeline behaviour::free_loop", "[audio][layer]" ) {
  reset_trigger();
  audio::layer layer("test_layer", audio::frame_count(4));
  layer.set_bahaviour(audio::layer::free_loop);
  
  layer.add_pass(triggered_pass(2));
  
  SECTION("empty") {
    REQUIRE_FALSE(layer.run_pipeline());
    REQUIRE(cb_trigger[2] == 0);
  }
  
  SECTION("ready") {
    auto source = generate_stereo(4);
    layer.load_audio(source.get(), 4, 2);
       
    REQUIRE_FALSE(layer.run_pipeline());
    REQUIRE(cb_trigger[2] == 0);
  }
  
  SECTION("active single window buffer") {
    auto source = generate_stereo(4);
    layer.load_audio(source.get(), 4, 2);
    layer.controller_start();
    REQUIRE(layer.state() == audio::layer::active);
    REQUIRE(layer.run_pipeline());
    REQUIRE(cb_trigger[2] == 1);
    REQUIRE(layer.state() == audio::layer::active);
    auto ep = layer.endpoint_reference();
    assert_stereo_window(
        ep.channel(audio::stereo_channel::left),
        ep.channel(audio::stereo_channel::right),
        source.get(),
        0,
        4
    );  

  }
  
  SECTION("active three window buffer") {
    auto source = generate_stereo(audio::frame_count(12)); // 3 periods
    layer.load_audio(source.get(), audio::frame_count(12), audio::channel_count(2));
    layer.controller_start();

    REQUIRE(layer.state() == audio::layer::active);
    
    for(auto count = 0u; count < 8; count++) {
      REQUIRE(layer.run_pipeline());
      auto ep = layer.endpoint_reference();
      assert_stereo_window(
          ep.channel(audio::stereo_channel::left),
          ep.channel(audio::stereo_channel::right),
          source.get(),
          count % 3 * audio::frame_count(4),
          4
      );
      
    }
    REQUIRE(cb_trigger[2] == 8);
    REQUIRE(layer.state() == audio::layer::active);
  }
}

TEST_CASE( "audio_layer.run_pipeline behaviour::sync_linear", "[audio][layer]" ) {
  reset_trigger();
  audio::layer layer("test_layer", audio::frame_count(4));
  layer.set_bahaviour(audio::layer::sync_linear);
  
  layer.add_pass(triggered_pass(2));
  
  SECTION("empty") {
    REQUIRE_FALSE(layer.run_pipeline());
    REQUIRE(cb_trigger[2] == 0);
  }
  
  SECTION("ready") {
    auto source = generate_stereo(audio::frame_count(8));
    layer.load_audio(source.get(), audio::frame_count(8), audio::channel_count(2));
       
    REQUIRE_FALSE(layer.run_pipeline());
    REQUIRE(cb_trigger[2] == 0);
  }
  
  SECTION("cued") {
    auto source = generate_stereo(audio::frame_count(8));
    layer.load_audio(source.get(), audio::frame_count(8), audio::channel_count(2));
    layer.controller_start();
    
    REQUIRE(layer.state() == audio::layer::cued);
    REQUIRE_FALSE(layer.run_pipeline());
    REQUIRE(cb_trigger[2] == 0);
  }

  SECTION("active") {
    auto source = generate_stereo(audio::frame_count(8));
    layer.load_audio(source.get(), audio::frame_count(8), audio::channel_count(2));
    layer.controller_start();
    layer.on_sync_start();
    REQUIRE(layer.state() == audio::layer::active);
  }

  SECTION("cycle single window buffer") {
    auto source = generate_stereo(4);
    layer.load_audio(source.get(), audio::frame_count(4), audio::channel_count(2));
    layer.controller_start();
    layer.on_sync_start();
    
    REQUIRE(layer.state() == audio::layer::active);
    REQUIRE(layer.run_pipeline());
    REQUIRE(cb_trigger[2] == 1);
    REQUIRE(layer.state() == audio::layer::ready);
      auto ep = layer.endpoint_reference();
      assert_stereo_window(
          ep.channel(audio::stereo_channel::left),
          ep.channel(audio::stereo_channel::right),
          source.get(),
          audio::frame_count(0),
          4
      );
  }
  
  SECTION("cycle 2 window buffer") {
    auto source = generate_stereo(audio::frame_count(8));
    layer.load_audio(source.get(), audio::frame_count(8), audio::channel_count(2));
    layer.controller_start();
    layer.on_sync_start();
    
    REQUIRE(layer.state() == audio::layer::active);
    auto count = 0;
    while(layer.state() == audio::layer::active) {
      REQUIRE(layer.run_pipeline());
      auto ep = layer.endpoint_reference();
      assert_stereo_window(
          ep.channel(audio::stereo_channel::left),
          ep.channel(audio::stereo_channel::right),
          source.get(),
          audio::frame_count(4) * count,
          4
      );
      count++;
    }
    REQUIRE(layer.state() == audio::layer::ready);
    REQUIRE(cb_trigger[2] == 2); // two windows of pipelines
    
  }
}

TEST_CASE( "audio_layer.run_pipeline behaviour::sync_loop", "[audio][layer]" ) {
  reset_trigger();
  auto period_size = audio::frame_count(4);
  audio::layer layer("test_layer", period_size);
  layer.set_bahaviour(audio::layer::sync_loop);
  
  layer.add_pass(triggered_pass(2));
  
  SECTION("empty") {
    REQUIRE_FALSE(layer.run_pipeline());
    REQUIRE(cb_trigger[2] == 0);
  }
  
  SECTION("ready") {
    auto source = generate_stereo(audio::frame_count(8));
    layer.load_audio(source.get(), audio::frame_count(8), audio::channel_count(2));
       
    REQUIRE_FALSE(layer.run_pipeline());
    REQUIRE(cb_trigger[2] == 0);
  }
  
  SECTION("cued") {
    auto source = generate_stereo(audio::frame_count(8));
    layer.load_audio(source.get(), audio::frame_count(8), audio::channel_count(2));
    layer.controller_start();
    
    REQUIRE(layer.state() == audio::layer::cued);
    REQUIRE_FALSE(layer.run_pipeline());
    REQUIRE(cb_trigger[2] == 0);
  }

  SECTION("active") {
    auto source = generate_stereo(audio::frame_count(8));
    layer.load_audio(source.get(), audio::frame_count(8), audio::channel_count(2));
    layer.controller_start();
    layer.on_sync_start();

    REQUIRE(layer.state() == audio::layer::active);
  }

  SECTION("cycle single window buffer") {
    auto source = generate_stereo(4);
    layer.load_audio(source.get(), audio::frame_count(4), audio::channel_count(2));
    layer.controller_start();
    layer.on_sync_start();
    
    REQUIRE(layer.state() == audio::layer::active);
    REQUIRE(layer.run_pipeline());
    REQUIRE(cb_trigger[2] == 1);
    REQUIRE(layer.state() == audio::layer::cued);
  }
  
  SECTION("cycle 3 window buffer") {
    auto buffer_size = period_size * 3;
    auto source = generate_stereo(buffer_size);
    layer.load_audio(source.get(), buffer_size, audio::channel_count(2));
    layer.controller_start();
    layer.on_sync_start();
    

    REQUIRE(layer.state() == audio::layer::active);
    
    auto count = 0;
    while(layer.state() == audio::layer::active) {
      REQUIRE(layer.run_pipeline()); // Read next window
      // Check output
      auto ep = layer.endpoint_reference();
      assert_stereo_window(
          ep.channel(audio::stereo_channel::left),
          ep.channel(audio::stereo_channel::right),
          source.get(),
          (count % 3) * audio::frame_count(4),
          4
      );

      count++;
    }
    
    REQUIRE(cb_trigger[2] == 3); // Should have run until end of buffer
    REQUIRE(layer.state() == audio::layer::cued); // Ready for next sync start
    
  }
}