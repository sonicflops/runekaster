#include "helper.hpp"

sfsw::audio::f32sample_uptr generate_stereo(sfsw::audio::frame_count num_frames) {
  auto buf = sfsw::audio::f32sample_uptr(new sfsw::audio::f32sample[num_frames * 2]);
  
  for(auto frame = 0u; frame < num_frames; frame++ ) {
    buf[frame * 2] = static_cast<float>(frame+1) + 0.10000000;
    buf[frame * 2 + 1] = static_cast<float>(frame+1) + 0.20000000;
  }
  
  return std::move(buf);
}

sfsw::audio::f32sample_uptr generate_mono(sfsw::audio::frame_count num_frames) {
  auto buf = sfsw::audio::f32sample_uptr(new sfsw::audio::f32sample[num_frames]);
  
  for(auto frame = 0u; frame < num_frames; frame++ ) {
    buf[frame] = static_cast<float>(frame+1) + 0.10000000;
  }
  
  return std::move(buf);
}

sfsw::audio::f32sample_uptr generate_zero_fill(sfsw::audio::frame_count num_frames, sfsw::audio::channel_count num_channels) {
  auto num_samples = num_frames * num_channels;
  auto buf = sfsw::audio::f32sample_uptr(new sfsw::audio::f32sample[num_samples]);
  
  
  for(auto sample = 0u; sample < num_samples; sample++ ) {
    buf[sample] = 0.0f;
  }
  
  return std::move(buf);
}

void assert_stereo_window(sfsw::audio::f32sample_wrk left,
                            sfsw::audio::f32sample_wrk right,
                            sfsw::audio::f32sample_wrk data,
                            sfsw::audio::frame_count data_from_frame,
                            sfsw::audio::frame_count total_frames) {
  
  auto data_sample = data_from_frame * 2; // frame * channels = sample
  
  for(auto frame = 0; frame < total_frames; frame++) {
    auto ls = data_sample + (frame*2);
    auto rs = ls+1;
    REQUIRE( left[frame] == Approx(data[ls]).epsilon(0.01) );
    REQUIRE( right[frame] == Approx(data[rs]).epsilon(0.01) );
  }
}

void assert_mono_window(sfsw::audio::f32sample_wrk left,
                            sfsw::audio::f32sample_wrk right,
                            sfsw::audio::f32sample_wrk data,
                            sfsw::audio::frame_count data_from_frame,
                            sfsw::audio::frame_count total_frames) {
  
  auto data_sample = data_from_frame; // frame * channels = sample
  for(auto frame = 0; frame < total_frames; frame++) {
    REQUIRE( left[frame] == Approx(data[data_sample]).epsilon(0.01) );
    REQUIRE( right[frame] == Approx(data[data_sample]).epsilon(0.01) );
    data_sample++;
  }
}

void debug_print_stereo_frames(sfsw::audio::f32sample_wrk left,
                        sfsw::audio::f32sample_wrk right,
                        sfsw::audio::frame_count total_frames) {
  std::cout << "Left\t\tRight\n----\t\t-----\n";
  for( auto frame = 0u; frame < total_frames; frame++) {
    std::cout << left[frame] << "\t\t" << right[frame] << "\n";
  }
  std::cout << std::endl;
}