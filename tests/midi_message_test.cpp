#include "catch.hpp"
#include "midi_message.hpp"

using namespace sfsw::midi;

TEST_CASE( "midi_message construct voice note_on", "[midi][message]" ) {
  std::uint8_t raw[3] = { 0x9b, 0x41, 0xff };
  message m(raw);
  
  REQUIRE( m.status() == status_code::note_on );
  REQUIRE( m.channel() == 0xb );
  REQUIRE( m.key() == 0x41 );
  REQUIRE( m.velocity() == 0xff );
}

TEST_CASE( "midi_message construct voice note_off", "[midi][message]" ) {
  std::uint8_t raw[3] = { 0x8b, 0x41, 0xff };
  message m(raw);
  
  REQUIRE( m.status() == status_code::note_off );
  REQUIRE( m.channel() == 0xb );
  REQUIRE( m.key() == 0x41 );
  REQUIRE( m.velocity() == 0xff );
}

TEST_CASE( "midi_message construct voice poly_key_pressure", "[midi][message]" ) {
  std::uint8_t raw[3] = { 0xab, 0x41, 0xff };
  message m(raw);
  
  REQUIRE( m.status() == status_code::poly_key_pressure );
  REQUIRE( m.channel() == 0xb );
  REQUIRE( m.key() == 0x41 );
  REQUIRE( m.pressure() == 0xff );
}

TEST_CASE( "midi_message construct voice control_change", "[midi][message]" ) {
  std::uint8_t raw[3] = { 0xbc, 0x41, 0xff };
  message m(raw);
  
  REQUIRE( m.status() == status_code::control_change );
  REQUIRE( m.channel() == 0xc );
  REQUIRE( m.controller() == 0x41 );
  REQUIRE( m.value() == 0xff );
}

TEST_CASE( "midi_message construct voice program_change", "[midi][message]" ) {
  std::uint8_t raw[3] = { 0xcb, 0x41, 0xff };
  message m(raw);
  
  REQUIRE( m.status() == status_code::program_change );
  REQUIRE( m.channel() == 0xb );
  REQUIRE( m.program() == 0x41 );
}

TEST_CASE( "midi_message construct voice channel_pressure", "[midi][message]" ) {
  std::uint8_t raw[3] = { 0xdb, 0x41, 0xff };
  message m(raw);
  
  REQUIRE( m.status() == status_code::channel_pressure );
  REQUIRE( m.channel() == 0xb );
  REQUIRE( m.pressure() == 0x41 );
}

/**
 * @todo:
 * Handle the pitch bend message
 */
TEST_CASE( "midi_message construct voice pitch_bend", "[midi][message]" ) {
  std::uint8_t raw[3] = { 0xeb, 0x41, 0xff };
  message m(raw);
  
  REQUIRE( m.status() == status_code::pitch_bend );
  
}

TEST_CASE( "midi_message construct channel reset_all_controllers", "[midi][message]" ) {
  std::uint8_t raw[3] = { 0x79, 0x00, 0x00 };
  message m(raw);
  
  REQUIRE( m.status() == status_code::reset_all_controllers );
}

TEST_CASE( "midi_message construct channel local_control", "[midi][message]" ) {
  
  SECTION("toggled on") {
    std::uint8_t raw[3] = { 0x7a, 0xff, 0x00 };
    message m(raw);

    REQUIRE( m.status() == status_code::local_control );
    REQUIRE( m.toggled() );
  }
  SECTION("toggled off") {
    std::uint8_t raw[3] = { 0x7a, 0x00, 0x00 };
    message m(raw);

    REQUIRE( m.status() == status_code::local_control );
    REQUIRE_FALSE( m.toggled() );
  }
}



TEST_CASE( "midi_message construct channel all_notes_off", "[midi][message]" ) {
  std::uint8_t raw[3] = { 0x7b, 0x00, 0x00 };
  message m(raw);
  
  REQUIRE( m.status() == status_code::all_notes_off );
}

TEST_CASE( "midi_message construct channel omni_off", "[midi][message]" ) {
  std::uint8_t raw[3] = { 0x7c, 0x00, 0x00 };
  message m(raw);
  
  REQUIRE( m.status() == status_code::omni_off );
}

TEST_CASE( "midi_message construct channel omni_on", "[midi][message]" ) {
  std::uint8_t raw[3] = { 0x7d, 0x00, 0x00 };
  message m(raw);
  
  REQUIRE( m.status() == status_code::omni_on );
}

TEST_CASE( "midi_message construct channel mono_on", "[midi][message]" ) {
  std::uint8_t raw[3] = { 0x7e, 0x00, 0x00 };
  message m(raw);
  
  REQUIRE( m.status() == status_code::mono_on );
}

TEST_CASE( "midi_message construct channel poly_on", "[midi][message]" ) {
  std::uint8_t raw[3] = { 0x7f, 0x00, 0x00 };
  message m(raw);
  
  REQUIRE( m.status() == status_code::poly_on );
}