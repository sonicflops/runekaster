#include <future>

#include "catch.hpp"
#include "jack_client.hpp"
using namespace sfsw::jack;

static int jprocess(jack_nframes_t nframes, void* data) {
  return 0;
}

static int rk6_process(jack_nframes_t nframes, void* data) {
  auto val = static_cast<int*>(data);
  *val = 5001;
  return 0;
}

TEST_CASE( "client open", "[jack][client]" ) {
  client client("rk1_observer");
  REQUIRE(client.open(nullptr,nullptr));
  auto ptr = jack_get_uuid_for_client_name(client.jack_ptr(), "rk1_observer");
  REQUIRE( ptr != nullptr );
  
}

TEST_CASE( "client close", "[jack][client]" ) {
  client oclient("rk2_observer");
  REQUIRE(oclient.open(nullptr,nullptr));
  
  {
    client sclient("rk2_subject"); // Scoped client will close resources
    sclient.open(nullptr,nullptr);
    auto ptr = jack_get_uuid_for_client_name(oclient.jack_ptr(), "rk2_subject");
    REQUIRE( ptr != nullptr );
  }
  
  auto ptr = jack_get_uuid_for_client_name(oclient.jack_ptr(), "rk2_subject");
  REQUIRE( ptr == nullptr );
}

TEST_CASE( "client.ports().register_audio_outputs", "[jack][client]" ) {
  client client("rk4_observer");
  REQUIRE(client.open(jprocess,nullptr));
  REQUIRE(client.ports().register_audio_outputs("output")); 
  
  auto ptr_left = jack_port_by_name(client.jack_ptr(), "rk4_observer:output_l");
  REQUIRE( ptr_left != nullptr );
  auto str_left = std::string(jack_port_type(ptr_left));
  REQUIRE(str_left == std::string(JACK_DEFAULT_AUDIO_TYPE));
  
  auto ptr_right = jack_port_by_name(client.jack_ptr(), "rk4_observer:output_l");
  REQUIRE( ptr_right != nullptr );
  auto str_right = std::string(jack_port_type(ptr_right));
  REQUIRE(str_right == std::string(JACK_DEFAULT_AUDIO_TYPE));
}


TEST_CASE( "client.ports().register_midi_input", "[jack][client]" ) {
  client client("rk5_observer");
  REQUIRE(client.open(jprocess,nullptr));
  REQUIRE(client.ports().register_midi_input("midi_in")); 

  SECTION("sanity") {
    REQUIRE(jack_port_by_name(client.jack_ptr(), "rk5_observer:invalid") == nullptr);
    REQUIRE(client.ports().midi_input("invalid") == nullptr);
  }
  
  SECTION("invalid") {
    REQUIRE_FALSE(client.ports().register_midi_input("midi_in")); // Duplicate name
  }
  
  SECTION("valid") {
    auto ptr = jack_port_by_name(client.jack_ptr(), "rk5_observer:midi_in");
    REQUIRE( ptr != nullptr );
    auto str = std::string(jack_port_type(ptr));
    REQUIRE(str == std::string(JACK_DEFAULT_MIDI_TYPE));
    
    REQUIRE(client.ports().midi_input("midi_in") != nullptr);
  }
}

TEST_CASE( "client.set_process_data", "[jack][client]" ) {
  client client("rk6_observer");
  int check_value = 5000; 
  client.set_process_data(static_cast<void*>(&check_value));
  
  REQUIRE(client.open(rk6_process,nullptr));
  REQUIRE(client.activate());
  
  std::this_thread::sleep_for(std::chrono::milliseconds(200));
  REQUIRE(check_value == 5001);  
}

TEST_CASE( "client.audio_output", "[jack][client]" ) {
  client client("rk6_observer");
  client.open(nullptr,nullptr);
  client.ports().register_audio_outputs("output");
  SECTION("absolute invalid") {
    REQUIRE(client.ports().audio_output("output_i") == nullptr);
  }
  
  SECTION("absolute left") {
    auto ptr = client.ports().audio_output("output_l");
    REQUIRE(ptr != nullptr);
    auto str = std::string(jack_port_name(ptr));
    REQUIRE(str == "rk6_observer:output_l");
    REQUIRE(std::string(jack_port_type(ptr)) == std::string(JACK_DEFAULT_AUDIO_TYPE));
  }
  
  SECTION("absolute right") {
    auto ptr = client.ports().audio_output("output_r");
    REQUIRE(ptr != nullptr);
    auto str = std::string(jack_port_name(ptr));
    REQUIRE(str == "rk6_observer:output_r");
    REQUIRE(std::string(jack_port_type(ptr)) == std::string(JACK_DEFAULT_AUDIO_TYPE));
  }
  
  SECTION("channel invalid") {
    REQUIRE(client.ports().audio_output("output_i", sfsw::audio::stereo_channel::left) == nullptr);
  }

  SECTION("channel left") {
    auto ptr = client.ports().audio_output("output", sfsw::audio::stereo_channel::left);
    REQUIRE(ptr != nullptr);
    auto str = std::string(jack_port_name(ptr));
    REQUIRE(str == "rk6_observer:output_l");
    REQUIRE(std::string(jack_port_type(ptr)) == std::string(JACK_DEFAULT_AUDIO_TYPE));
  }
  
  SECTION("channel right") {
    auto ptr = client.ports().audio_output("output", sfsw::audio::stereo_channel::right);
    REQUIRE(ptr != nullptr);
    auto str = std::string(jack_port_name(ptr));
    REQUIRE(str == "rk6_observer:output_r");
    REQUIRE(std::string(jack_port_type(ptr)) == std::string(JACK_DEFAULT_AUDIO_TYPE));
  }
}

TEST_CASE( "client.rename_audio_outputs", "[jack][client]" ) {
  client client("rk8_observer");
  REQUIRE(client.open(nullptr,nullptr));
  REQUIRE(client.ports().register_audio_outputs("initial"));
  
  // Sanity
  REQUIRE(jack_port_by_name(client.jack_ptr(), "rk8_observer:initial_l") != nullptr);
  REQUIRE(jack_port_by_name(client.jack_ptr(), "rk8_observer:initial_r") != nullptr);
  
  REQUIRE(client.ports().rename_audio_outputs("initial", "subsequent"));

  REQUIRE(jack_port_by_name(client.jack_ptr(), "rk8_observer:initial_l") == nullptr);
  REQUIRE(jack_port_by_name(client.jack_ptr(), "rk8_observer:initial_r") == nullptr);
  
  REQUIRE(jack_port_by_name(client.jack_ptr(), "rk8_observer:subsequent_l") != nullptr);
  REQUIRE(jack_port_by_name(client.jack_ptr(), "rk8_observer:subsequent_r") != nullptr);
}

TEST_CASE( "client.ports().unregister_audio_outputs", "[jack][client]" ) {
  client client("rk10_observer");
  REQUIRE(client.open(nullptr,nullptr));
  REQUIRE(client.ports().register_audio_outputs("output"));
  
  SECTION("sanity") {
    REQUIRE(jack_port_by_name(client.jack_ptr(), "rk10_observer:output_l") != nullptr);
    REQUIRE(jack_port_by_name(client.jack_ptr(), "rk10_observer:output_r") != nullptr);
    REQUIRE(client.ports().audio_output("output_l") != nullptr);
    REQUIRE(client.ports().audio_output("output_r") != nullptr);
  }
  
  SECTION("invalid") {
    REQUIRE_FALSE(client.ports().unregister_audio_outputs("invalid"));
  }
  
  SECTION("valid") {
    REQUIRE(client.ports().unregister_audio_outputs("output"));
    REQUIRE(jack_port_by_name(client.jack_ptr(), "rk10_observer:output_l") == nullptr);
    REQUIRE(jack_port_by_name(client.jack_ptr(), "rk10_observer:output_r") == nullptr);
    REQUIRE(client.ports().audio_output("output_l") == nullptr);
    REQUIRE(client.ports().audio_output("output_r") == nullptr);

  }
}

TEST_CASE( "client.rename_midi_input", "[jack][client]" ) {
  client client("rk11_observer");
  REQUIRE(client.open(jprocess,nullptr));
  REQUIRE(client.ports().register_midi_input("midi_in")); 
  
  SECTION("sanity") {
    REQUIRE(jack_port_by_name(client.jack_ptr(), "rk11_observer:midi_in") != nullptr);
    REQUIRE(client.ports().midi_input("midi_in") != nullptr);
  }
  
  SECTION("invalid") {
    REQUIRE_FALSE(client.ports().rename_midi_input("invalid", "foobar"));
    REQUIRE_FALSE(client.ports().rename_midi_input("midi_in", "midi_in"));
  }
  
  SECTION("valid") {
    REQUIRE(client.ports().rename_midi_input("midi_in", "next_in"));
    REQUIRE(jack_port_by_name(client.jack_ptr(), "rk11_observer:midi_in") == nullptr);
    REQUIRE(client.ports().midi_input("midi_in") == nullptr);
    REQUIRE(jack_port_by_name(client.jack_ptr(), "rk11_observer:next_in") != nullptr);
    REQUIRE(client.ports().midi_input("next_in") != nullptr);
  }
}

TEST_CASE( "client.ports().unregister_midi_input", "[jack][client]" ) {
  client client("rk12_observer");
  REQUIRE(client.open(jprocess,nullptr));
  REQUIRE(client.ports().register_midi_input("midi_in"));
  
  SECTION("sanity") {
    REQUIRE(jack_port_by_name(client.jack_ptr(), "rk12_observer:midi_in") != nullptr);
    REQUIRE(client.ports().midi_input("midi_in") != nullptr);
  }
  
  SECTION("invalid") {
    REQUIRE_FALSE(client.ports().unregister_midi_input("invalid"));
  }
  
  SECTION("valid") {
    REQUIRE(client.ports().unregister_midi_input("midi_in"));
    REQUIRE(jack_port_by_name(client.jack_ptr(), "rk12_observer:midi_in") == nullptr);
    REQUIRE(client.ports().midi_input("midi_in") == nullptr);
  }
}