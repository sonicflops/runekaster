#include "workspace.hpp"

using namespace sfsw;

workspace::workspace()
  : _jack_client("runekaster")
  , _stream()
{
  // Create a default active locus
  add_locus("main");
  _active_locus = 0;
  
}

workspace::workspace(audio::stream_info info)
  : _jack_client("runekaster")
  , _stream(info)
{ 
  // Create a default active locus
    add_locus("main");
    _active_locus = 0;
}

workspace::~workspace() {
}

audio::locus& workspace::system_active_locus() {
  return _loci[_active_locus];
}

jack::client& workspace::system_jack_client() {
  return _jack_client;
}

midi::event_bus& workspace::system_midi_bus() {
  return _midi_bus;
}

void workspace::init() {

}

void workspace::add_locus(std::string label) noexcept {
  _loci.push_back(audio::locus(label, _stream));
}

void workspace::add_layer(std::string label) noexcept {
  _loci[_active_locus].add_layer(label);
  _jack_client.ports().register_audio_outputs(label);
}

bool workspace::remove_layer(std::string label) noexcept {
  
  if(!system_active_locus().remove_layer(label)) {
    return false;
  }
  system_jack_client().ports().unregister_audio_outputs(label);
  return true;
}

bool workspace::relabel_layer(std::string old_label, std::string new_label) noexcept {
  
  if(!system_active_locus().relabel_layer(old_label, new_label)) { 
    return false; 
  }
  
  system_jack_client().ports().rename_audio_outputs(old_label, new_label);
  return true;
}

bool workspace::create_layer_sync(std::string subject, std::string observer, sync_type type) noexcept {
  try {
    auto& s = layer_from_label(subject);
    auto& o = layer_from_label(observer);
    
    switch(type) {
      case workspace::sync_start:
        s.subscribe(
          s.event_sync_start, 
          delegate_0(o, &audio::layer::on_sync_start)
        );
        break;
      default:
        return false;
    }
  } catch(std::exception& e) {
    std::cerr << e.what() << "\n";
    return false;
  }
  
  return true;
}

bool workspace::drop_layer_sync(std::string subject, std::string observer) {
  try {
    auto& s = layer_from_label(subject);
    auto& o = layer_from_label(observer);
    s.unsubscribe(s.event_sync_start, o);
    s.unsubscribe(s.event_sync_bar, o);
  } catch(std::exception& e) {
    std::cerr << e.what() << "\n";
    return false;
  }
  
  return true;
}


audio::layer& workspace::layer_from_label(std::string label) {
  for(auto& l : system_active_locus().endpoints()) {
    if(l.label() == label){ return l; }
  }
  throw std::out_of_range("layer does not exist in locus");
}

