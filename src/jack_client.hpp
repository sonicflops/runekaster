#ifndef JCLIENT_HPP
#define JCLIENT_HPP
#include <functional>

#include "jack_port_container.hpp"

namespace sfsw { namespace jack {
  
  using jcb_process = int(*)(jack_nframes_t, void*);
  using jcb_shutdown = void(*)(void*);

  
class client {
public:
  client(std::string name);
  client(const client& orig);
  virtual ~client();

  /**
   * Open a connection with the jack server
   * 
   * @param process Function pointer to process callback
   * @param shutdown Function pointer to shutdown callback
   * @return True if successfully opened
   */
  bool open(jcb_process process, jcb_shutdown shutdown);
  
  /**
   * Close the jack client connection
   */
  void close();
  
  /**
   * Activate the jack client with the server
   * 
   * @return True if activated successfully
   */
  bool activate();
  
  /**
   * Deactivate the jack client with the server
   * 
   * @return True if deactivated successfully
   */
  bool deactivate();
  
  jack_nframes_t samplerate();
  
  /**
   * Set the data that will be passed to the process callback function
   * @param data Pointer to the data cast to a void
   */
  void set_process_data(void* data);
  
  /**
   * Get the pointer to the jack client
   * 
   * @return 
   */
  jack_client_t* jack_ptr();

  
  /**
   * Get a reference to the port container
   * 
   * @return port_container& reference
   */
  port_container& ports();
  
private:
  std::string _name;
  char *_sname;

  port_container _ports;

  jack_client_t *_client;
  jack_status_t  _status;
  
  jcb_process _cb_process;
  jcb_shutdown _cb_shutdown;
  
  void* _process_data;

};

} }
#endif /* JCLIENT_HPP */

