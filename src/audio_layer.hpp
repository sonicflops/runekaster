#ifndef AUDIO_LAYER_HPP
#define AUDIO_LAYER_HPP



#include "event.hpp"
#include "audio_buffer.hpp"
#include "audio_endpoint.hpp"

#include "labeled.hpp"
#include "uid.hpp"


namespace sfsw { namespace audio {
  

class layer
  : public labeled {
public:
  class event_sync_start_t : public event<> { } event_sync_start;
  class event_sync_bar_t : public event<frame_count> { } event_sync_bar;
  
  typedef std::vector<pass::signal_pass> audio_pipeline;
  
  enum runtime_state {
    active, cued, ready, empty,
  };
  
  enum runtime_behaviour {
    free_loop, free_linear, sync_loop, sync_linear,
  };
  
public:
  layer(std::string label);
  layer(std::string label, frame_count period_size);
  
  layer(layer&& orig);
  
  
  virtual ~layer();
  
  void notify_test_start() {
    notify_sync_start();
  };
  
  void notify_test_bar() {
    notify_sync_bar();
  };
  
  /**
   * Get the current runtime state of layer
   * 
   * @return runtime_state
   */
  runtime_state state();
  
  /**
   * Get the current runtime behaviour of layer
   * @return 
   */
  runtime_behaviour behaviour();
  
  /**
   * Set the runtime behaviour of the layer
   * @param behaviour The behaviour to set to
   */
  void set_bahaviour(runtime_behaviour behaviour);
  
  /**
   * Subscribe a delegate to an event
   * 
   * @param ev The event to subscribe to
   * @param delegate The delegate for the event
   */
  template<typename E, typename D>
  void subscribe(E& ev, D delegate) {
    ev.subscribe(delegate);
  }

  /**
   * Unsubscribe from an event
   * 
   * @param ev The event to unsubscribe from
   * @param observer The observer that is unsubscribing
   */
  template<typename E, class O>
  void unsubscribe(E& ev, O&& observer) {
    ev.unsubscribe(std::forward<O>(observer));
  }
  
  void on_sync_start();
  void on_sync_bar(frame_count diff);

  
  /**
   * Get the name of the layer's endpoint
   * 
   * This is just the label of the layer
   * 
   * @return std::string name of end point
   */
  std::string endpoint_name();
  
  /**
   * Get the reference to the end point for the layer
   * 
   * @return const &endpoint
   */
  const endpoint& endpoint_reference();
  
  
  /**
   * Load audio from file into buffer
   * 
   * @param path Path to the file
   */
  void load_audio(std::string path);
  
  /**
   * Load audio from memory into buffer
   * 
   * If there is one channel then the samples represent one contiguous
   * 
   * If there are two channels then the samples are assumed to be interleaved
   * 
   * @param data Working pointer to the memory
   * @param frames Number of frames to copy
   * @param channels Number of channels
   */
  void load_audio(f32sample_wrk data, frame_count frames, channel_count channels = 2);
  
  /**
   * Notify the layer has stopped, reset the buffer
   */
  void controller_stop();
  
  /**
   * Notify the layer to start playing
   */
  void controller_start();
  
  /**
   * Run the audio processing pipeline
   */
  bool run_pipeline();
  
  /**
   * Add a pass to the pipeline
   * @param op The signal pass functor
   */
  void add_pass(pass::signal_pass op);
  
  /**
   * Get a reference to the pipeline
   * 
   * @todo this is lazy
   * 
   * @return audio_pipeline&
   */
  audio_pipeline& pipeline();
  
  /**
   * Get the system UID of object
   * 
   * This is used for registering as an observer
   * 
   * @return The unique ID
   */
  sfsw::uid::unique_id uid();

private:

  endpoint _endpoint;             ///< End point used when interface with jack ports
  buffer_uptr _buffer;            ///< Buffer of sampes
  signal _signal;                 ///< Signal to run through the pipeline
  
  runtime_state _state;           ///< Current state of the layer
  runtime_behaviour _behaviour;   ///< Current runtime behaviour of layer
  bool _runnable;                 ///< Flag to indicate the layer is runable
  frame_count _prefill;           ///< Number of frame to zero fill
  
  audio_pipeline _pipeline; ///< Audio processing pipeline
  
  uid::unique_id _uid;    ///< System wide UUID for this object
  /**
   * Notify observers to sync with the start
   */
  void notify_sync_start();
  
  /**
   * Notify observers to sync with the bar
   */
  void notify_sync_bar();
  
  /**
   * Set the current runtime state
   * 
   * @todo Make the runnable state atomic
   * 
   * @param state State to change to
   */
  inline void set_state(runtime_state state) {
    _state = state;
    switch(state) {
      case runtime_state::active:
      case runtime_state::cued:
      case runtime_state::ready:
        _runnable = true;
      default:
        _runnable = false;
    }
  }
  
  /**
   * Dereference buffer_uptr and get a reference to the internal buffer
   * 
   * @return reference to buffer
   */
  inline buffer& buf() {
    return *_buffer;
  }
  
  /**
   * Run a zero fill operation if required
   * @param signal The signal to prefill
   */
  inline void prefill(signal& signal) {
    if(_prefill > 0) {
      signal.pass(pass::zero_fill(_prefill));
      _prefill = 0;
    }
  }
  
  /**
   * Change the runtime state based on the behaviour
   * 
   * @todo: Potential race conditions everywhere!
   */
  inline void resolve_state() {
    
    // Check if a state resolution is even necessary
    if(buf().state() != buffer::ready){ return; }
    
    switch(behaviour()) {
      
      // Reached the end of a linear play
      // So wait to become active again
      case runtime_behaviour::free_linear:
      case runtime_behaviour::sync_linear:
        set_state(runtime_state::ready);
        break;
      
      // Reached the end of a syncronised loop
      // so wait for the next notification
      case runtime_behaviour::sync_loop:
        set_state(runtime_state::cued);
        break;
        
      // Reached end of a free loop so just
      // continue from the beginning
      case runtime_behaviour::free_loop:
        set_state(runtime_state::active);
        break;
    }
  }
};

} }
#endif /* AUDIO_LAYER_HPP */

