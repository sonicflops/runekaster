/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   audio_attr.hpp
 * Author: cfg
 *
 * Created on 25 February 2017, 12:28
 */

#ifndef AUDIO_ATTR_HPP
#define AUDIO_ATTR_HPP
namespace sfsw { namespace audio { namespace attr {
    /**
     * Type for specifying direction of positioning
     */
    enum class position : int {
      forward,  ///< Move forward
      backward, ///< Move backward
      absolute, ///< Move to an absolute position
    };

} } }


#endif /* AUDIO_ATTR_HPP */

