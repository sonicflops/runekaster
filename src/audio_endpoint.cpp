#include "audio_endpoint.hpp"

using namespace sfsw::audio;

endpoint::endpoint()
  : _channel({nullptr, nullptr})
{ }

endpoint::endpoint(const endpoint& orig) 
  : _channel(orig._channel)
{ }

endpoint::~endpoint() {
}

const f32sample_wrk endpoint::channel(stereo_channel index) const {
  return _channel[ strong_cast(index) ];
}

void endpoint::operator()(signal& signal) {
  _channel[0] = signal.channel(stereo_channel::left);
  _channel[1] = signal.channel(stereo_channel::right);
}
