#ifndef AUDIO_LOADER_HPP
#define AUDIO_LOADER_HPP
#include "audio_buffer.hpp"
namespace sfsw{ namespace audio {

  buffer_uptr load_buffer(std::string path);

} }
#endif /* AUDIO_LOADER_HPP */

