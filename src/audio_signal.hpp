
#ifndef SIGNAL_HPP
#define SIGNAL_HPP
#include "common.hpp"

namespace sfsw { namespace audio {

  /**
   * A signal is a period sized window on the waveform
   */
class signal {
public:
  signal(frame_count period_size = SFSW_PERIOD_SIZE);
  signal(const signal& orig);
  ~signal();
  
  template<typename T>
  void pass(T& op) {
    op(*this);
  }
  
  template<typename T>
  void pass(T&& op) {
    op(*this);
  }

  /**
   * Get a working pointer to the data for a channel
   * 
   * @param index The channel index
   * @return f32sample_wrk
   */
  f32sample_wrk channel(stereo_channel index);
  
  /**
   * Get a working pointer to the data for a channel, synced with write head
   * 
   * This passes back the pointer at the position of the write head
   * 
   * @param index The channel index
   * @return f32sample_wrk
   */
  f32sample_wrk channel_synced(stereo_channel index);
  
  /**
   * Get number of frames in the signal period
   * 
   * @return frame_count
   */
  frame_count num_total();
  
  /**
   * Get the number of frames available to fill
   * 
   * If there has been a frame sync operation performed before the current
   * pass, then this will give the correct number of frames to fill
   * 
   * @return frame_count The number of frames
   */
  frame_count num_available();
  
  
  /**
   * Sync the write head to a frame position in the buffer
   * 
   * This is used for any frame frame fill operations on the signal before
   * having other passes performed on it
   * 
   * @param frame The number of frames to sync to
   */
  void sync_write_head(frame_count frame, attr::position direction);
  
   
  /**
   * Reset the position of the write head back to the first frame
   */
  void reset();
  
  /**
   * Get frame position of write head
   * 
   * @return frame_count The number of frames
   */
  frame_count position();
private:

  channel_container _channels; ///< A pair of channels
  frame_count _period_size; ///< Size of a period
  frame_count _write_head; ///< Current read head position (frame syncing)
  frame_count _available; ///< The number of frames avalailble to fill
};

} }
#endif /* SIGNAL_HPP */

