#ifndef CONTROLLERS_HPP
#define CONTROLLERS_HPP
#include "midi_event.hpp"
#include "jack_client.hpp"
#include "audio_locus.hpp"
namespace sfsw {

/**
 * Used for containing the necessary components for dealing with the interface
 * on the application boundry
 */
class interface_container {
 
public:
  
  /**
   * get a reference to current midi bus used by the system
   * 
   * @return midi::event_bus&
   */
  virtual midi::event_bus& system_midi_bus() = 0;
  
  /**
   * Get a pointer to the current jack client used by the system
   * @return jclient*
   */
  virtual jack::client& system_jack_client() = 0;
  
  /**
   * Get a reference to the active locus
   * 
   * @return audio::locus&
   */
  virtual audio::locus& system_active_locus() = 0;
};

}
#endif /* CONTROLLERS_HPP */

