#ifndef LOCUS_HPP
#define LOCUS_HPP
#include "audio_layer.hpp"
#include "labeled.hpp"

namespace sfsw{ namespace audio {

  /**
   * Represents the central point of activity
   * 
   * This class provides functionality for working with the current layers
   * of audio. Different loci provide different layers of audio
   */
  class locus 
    : public labeled {
  public:
    using layer_container = std::vector<layer>;
  public:
    locus(std::string label);
    locus(std::string label, stream_info info);
    locus(const locus& orig);
    virtual ~locus();

    /**
     * Create a new layer with given label
     * 
     * @param label Label for the layer
     */
    void add_layer(std::string label);
    
    /**
     * Remove a layer from the locus
     * 
     * @param label
     * @return True on successfully addition of layer
     */
    bool remove_layer(std::string label);
    
    /**
     * Rename the layer with old_name to new_name
     * 
     * @param old_label The layer to change
     * @param new_label The name to change to
     * @return True on successful renaming
     */
    bool relabel_layer(std::string old_label, std::string new_label);
    
    layer_container& endpoints();

  private:
    layer_container _layers; ///< Container of layers
    std::string _label; ///< Label attached to locus
    stream_info _stream; ///< The stream information
    

    /**
     * Check to see if there is a layer with the same label
     * 
     * @param label Label to to check against
     * @return True if it already exists
     */
    inline bool label_exist(std::string label) {
      for(auto& layer : _layers) {
        if(layer.label() == label){ return true; }
      }
      return false;
    }
    
  };

} }
#endif /* LOCUS_HPP */

