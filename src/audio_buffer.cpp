#include <deque>

#include "audio_buffer.hpp"

using namespace sfsw::audio;

buffer::buffer(frame_count num_frames, channel_count num_channels)
  : _behaviour(buffer::linear_iterator), _runtime_state(buffer::empty)
  , _frame_count(num_frames), _channel_count(num_channels)
  , _sample_count(num_frames * num_channels)
  , _buffer(f32sample_uptr(new f32sample[ num_frames * num_channels]))
  , _read_head(0)
{ }

buffer::buffer(buffer&& orig)
  : _behaviour(orig._behaviour), _runtime_state(orig._runtime_state)
  , _frame_count(orig._frame_count), _channel_count(orig._channel_count)
  , _sample_count(orig._sample_count)
  , _buffer(std::move(orig._buffer))
  , _read_head(orig._read_head)
{ }

buffer::~buffer() {
}

void buffer::copy_window(signal& signal) {
  set_state(runtime_state::active);
  _channel_count == 1 
    ? mono_fill(signal)
    : stereo_fill(signal);
  
  if(_behaviour == iterator_behaviour::linear_iterator
  && _read_head == _frame_count) {
    reset(runtime_state::ready);
  }
  
}

void buffer::set_behaviour(iterator_behaviour behaviour) {
  _behaviour = behaviour;
}

f32sample_wrk buffer::data() {
  return _buffer.get();
}

frame_count buffer::num_frames() {
  return _frame_count;
}

sample_count buffer::num_samples() {
  return _sample_count;
}

channel_count buffer::num_channels() {
  return _channel_count;
}

void buffer::operator()(signal& signal) {
  copy_window(signal);
}

void buffer::notify_data_ready() {
  _runtime_state = buffer::ready;
}

void buffer::notify_data_empty() {
  _runtime_state = buffer::empty;
}

buffer::runtime_state buffer::state() {
  return _runtime_state;
}

bool buffer::data_ready() {
  return _runtime_state == empty ? false : true;
}

void buffer::reset() {
  _read_head = 0;
}

void buffer::reset(runtime_state state) {
  _read_head = 0;
  _runtime_state = state;
}


void buffer::copy_from(f32sample_wrk data, frame_count num_frames) {
  auto total = num_frames * _channel_count;
  std::copy(data, data+total, _buffer.get());
  set_state(runtime_state::ready);
}

void buffer::seek(frame_count num_frames) {
  set_state(buffer::active);
  switch(_behaviour) {
    case iterator_behaviour::linear_iterator:
      if(_read_head + num_frames > _frame_count) {
        _read_head = _frame_count - 1;
        set_state(buffer::finished);
      } else {
        _read_head += num_frames;
      }
      break;
      
    case iterator_behaviour::looped_iterator:
      auto p = (_read_head + num_frames) % _frame_count;
      _read_head = p;
      break;
  }
}

void buffer::rewind(frame_count num_frames) {
  set_state(buffer::active);
  switch(_behaviour) {
    case iterator_behaviour::linear_iterator:
      if(_read_head - num_frames > _frame_count) {
        _read_head = 0;
        set_state(buffer::ready);
      } else {
        _read_head -= num_frames;
      }
      break;
      
    case iterator_behaviour::looped_iterator:
      auto p = (static_cast<signed int>(_read_head) - num_frames) % _frame_count;
      _read_head = p;
      break;
  }
}

frame_count buffer::position() {
  return _read_head;
}

void buffer::sync_read_head(frame_count frame, attr::position direction) {
  switch(direction) {
      case attr::position::forward:
        _read_head += frame;
        break;

      case attr::position::backward:
        _read_head -= frame;
        break;
      
      case attr::position::absolute:
        _read_head = frame;
        break;
    }
}












