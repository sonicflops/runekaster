#include <vector>
#include <map>

#include "jack_client.hpp"
using namespace sfsw::jack;

client::client(std::string name)
  : _name(name),  _sname(nullptr), _ports(nullptr)
{ }

client::client(const client& orig)
  : _ports(orig._ports)
{ }

client::~client() {
  deactivate();
  close();
}

bool client::open(jcb_process process, jcb_shutdown shutdown) {
  _client = jack_client_open(_name.c_str(), JackNullOption, &_status, _sname);
  if(!_client) {
    std::cerr << "jack_client_open() failed, " << "status=" << _status << "\n";
    if(_status * JackServerFailed) {
      std::cerr << "Unable to connecto to JACK server\n";
    }
    return false;
  }
  
  _ports.reset_client(_client);
  
  if(_status & JackServerStarted) {
    std::cerr << "JACK server started\n";
  }
  
  if(_status & JackNameNotUnique) {
    _name = std::string(jack_get_client_name(_client));
    std::cerr << "Name reassigned as " << _name << "\n";
  }

  jack_set_process_callback(_client, process, _process_data);

  if(shutdown) {
    jack_on_shutdown(_client, shutdown, nullptr);
  }
  
  return true;
}

void client::close() {
  if(_client == nullptr) { return; }
  jack_client_close(_client);
  _client = nullptr;
}

bool client::activate() {
  return jack_activate(_client) == 0 ? true : false;
}

bool client::deactivate() {
  if(_client == nullptr) { return true; }
  return jack_deactivate(_client) == 0 ? true : false;
}



jack_nframes_t client::samplerate() {
  return jack_get_sample_rate(_client);
}

void client::set_process_data(void* data) {
  _process_data = data;
}

jack_client_t* client::jack_ptr() {
  return _client;
}

port_container& client::ports() {
  return _ports;
}