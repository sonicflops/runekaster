#ifndef MIDI_MSG_HPP
#define MIDI_MSG_HPP
#include "common.hpp"

namespace sfsw { namespace midi {
  
  /**
   * Status codes of MIDI message
   */
  enum class status_code {
    /// Voice Messages
    note_on,note_off,poly_key_pressure,control_change,program_change,channel_pressure,pitch_bend,
    
    /// Channel Messages
    reset_all_controllers, local_control, all_notes_off, omni_on, omni_off, mono_on, poly_on,
    
    /// Misc
     unknown,
  };
 
  using data_b1 = unsigned int;
  using data_b2 = unsigned int;
  
  /**
   * Encapsulate a MIDI message
   * 
   * A message is built from one status bytes and two
   * data bytes
   * 
   * @todo:
   * Handle the pitch bend message byte 1 and byte 2
   */
  class message {
  public:
    
    /**
     * Create a new MIDI message
     * 
     * @param msg The three byte raw data
     */
    message(std::uint8_t* msg)
    : _b0(msg[0]), _b1(msg[1]), _b2(msg[2])
    { }
  public:
    
    /**
     * Get the status code of the message
     * 
     * This information is relevant to all messages
     * 
     * For voice messages it comes from the 4 MSBs of byte 0
     * 
     * For channel messages it comes from the whole of byte 0
     *
     * @return status_code
     */
    inline status_code status() const {
      switch(_b0 & 0xf0) {
        /// Voice messages
        case 0x80: return status_code::note_off;
        case 0x90: return status_code::note_on;
        case 0xa0: return status_code::poly_key_pressure;
        case 0xb0: return status_code::control_change;
        case 0xc0: return status_code::program_change;
        case 0xd0: return status_code::channel_pressure;
        case 0xe0: return status_code::pitch_bend;
        
        // Channel messages
        case 0x70: switch(_b0 & 0x0f) {
          case 0x09: return status_code::reset_all_controllers;
          case 0x0a: return status_code::local_control;
          case 0x0b: return status_code::all_notes_off;
          case 0x0c: return status_code::omni_off;
          case 0x0d: return status_code::omni_on;
          case 0x0e: return status_code::mono_on;
          case 0x0f: return status_code::poly_on;
        }
      }
      
      return status_code::unknown;
    }
    
    /**
     * Get the channel ID of message
     * 
     * This information is relevant to all voice messages and comes from the 4 
     * LSBs of byte 0
     * 
     * @return unsigned int
     */
    inline unsigned int channel() const {
      return static_cast<unsigned int>(_b0 & 0x0f);
    }
    
    /**
     * Get the MIDI key number
     * 
     * This information is relevant to: note_on, note_off, poly_key_pressure
     * 
     * It comes from byte 1
     * 
     * @return  unsigned int
     */
    inline unsigned int key() const {
      return static_cast<unsigned int>(_b1);
    }
    
    /**
     * Get the MIDI controller number
     * 
     * This information is relevant to: control_change
     * 
     * It comes from byte 1
     * 
     * @return unsigned int
     */
    inline unsigned int controller() const {
      return static_cast<unsigned int>(_b1);
    }
    
    /**
     * Get the program number
     * 
     * This information is relevant to: program_change
     * 
     * It comes from byte 1
     * 
     * @return unsigned int
     */
    inline unsigned int program() const {
      return static_cast<unsigned int>(_b1);
    }
    
    /**
     * Get the controller value
     * 
     * This information is relevant to: controller_change
     * 
     * It comes from byte 2
     * 
     * @return unsigned int
     */
    inline unsigned int value() const {
      return static_cast<unsigned int>(_b2);
    }
    
    /**
     * Get the note velocity
     * 
     * This information is relevant to: note_on, note_off
     * 
     * It comes from byte 2
     * 
     * @return unsigned int
     */
    inline unsigned int velocity() const {
      return static_cast<unsigned int>(_b2);
    }
    
    /**
     * Get the pressure value
     * 
     * This information is relevant to: poly_key_pressure, channel_pressure
     * 
     * It comes from byte 2 for poly_key_pressure
     * It comes from byte 1 for channel_pressure
     * 
     * @return unsigned int
     */
    inline unsigned int pressure() const {
      if( status() == status_code::channel_pressure) {
        return static_cast<unsigned int>(_b1);
      }
      
      // poly_key_pressure
      return static_cast<unsigned int>(_b2);
    }
    
    /**
     * Check whether control is on or off
     * 
     * This information is relevant to: local_control
     * 
     * It comes from byte 2
     * @return bool
     */
    inline bool toggled() const {
      return _b1 > 0 ? true : false;
    }
    
    /**
     * Get the message byte at the index 0 - 2
     * 
     * If the index is greater than 2, it will just return the last byte
     * 
     * @param index Index of the byte
     * @return uint8_t
     */    
    inline uint8_t byte(unsigned int index) const {
      switch(index) {
        case 0: return _b0;
        case 1: return _b1;
        case 2: return _b2;
        default: return _b2;
      }
    }
    
    /**
     * Get the message byte at specified index
     * 
     * If the index is greater than 2, it will just return the last byte
     * 
     * @param index Index of byte
     * @return uint8_t
     */
    inline uint8_t operator[](int index) const {
      return byte(index);
    }
    
  private:
    std::uint8_t _b0, _b1, _b2;
  };
  
//  /**
//   * Convert a midi status to a string representation
//   * 
//   * @param s The status
//   * @return string
//   */
//  std::string status_string(status_code s) {
//    switch(s) {
//      case status_code::note_on:            return "note_on";
//      case status_code::note_off:           return "note_off";
//      case status_code::channel_pressure:   return "channel_pressure";
//      case status_code::control_change:     return "control_change";
//      case status_code::pitch_bend:         return "pitch_bend";
//      case status_code::poly_key_pressure:  return "poly_key_pressure";
//      case status_code::program_change:     return "program_change";
//      default:                              return "invalid";
//    }
//  }
  
} }
#endif /* MIDI_MSG_HPP */

