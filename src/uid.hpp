#ifndef UUID_HPP
#define UUID_HPP
#include <atomic>
namespace sfsw { namespace uid {
  
  using unique_id = unsigned int;
  unique_id generate();
  
}  }


#endif /* UUID_HPP */

