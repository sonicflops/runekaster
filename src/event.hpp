#ifndef EVENT_HPP
#define EVENT_HPP
#include <list>
#include <functional>
#include <map>

#include "uid.hpp"
namespace sfsw {
  
  /**
   * The base template for events used in the system
   * 
   * Current each event has it's own list of subscribers
   */
  template<typename ... T>
  class event {
  public:
    typedef sfsw::uid::unique_id delegate_value;
    using callback = std::function<void(T...)>;
    

  private:
    using callback_pair = std::pair<delegate_value, callback>;
    using callback_map = std::map<delegate_value, callback>;
    typedef void(fnt)(T...);

  public:
    void notify(T ... values) {
      for(auto& f : _callbacks) {
        f.second(values...);
      }
    }

    template<typename D>
    void subscribe(D delegate) {
      
      _callbacks.insert(
        callback_pair(
          delegate.observer,
          delegate.callback
        )
      );
    }
    
    template<class O>
    void unsubscribe(O& ptr) {
      auto it = _callbacks.find(ptr.uid());
      if(it != _callbacks.end()) {
        _callbacks.erase(it);
      }
    }
    protected:
      callback_map _callbacks;
  };
 
 
  /**
   * Represents an observer delegate for event handling
   * 
   * @param o Reference to the observer object
   * @param f Reference to the delegate function
   */
  template<typename F>
  struct delegate {
    template<class O>
    delegate(O& o, F f)
      : observer(o.uid()), callback(f) 
    {  }
    event<>::delegate_value observer;
    F callback;
  };
  
  /// @todo This is surely done with templates??
  
  /**
   * Construct a delegate with zero variable signature
   * 
   * @param o Reference to observer
   * @param f Reference to delegate member function
   * @return delegate
   */
  template<class O, typename F>
  auto inline delegate_0(O& o, F&& f) {
    auto b = std::bind(std::forward<F>(f), &o);
    return delegate<decltype(b)>(o, b);
  }

  /**
   * Construct a delegate with one variable signature
   * 
   * @param o Reference to observer
   * @param f Reference to delegate member function
   * @return delegate
   */
  template<class O, typename F>
  auto inline delegate_1(O& o, F&& f) {
    auto b = std::bind(std::forward<F>(f), o, std::placeholders::_1);
    return delegate<decltype(b)>(o, b);
  }

  /**
   * Construct a delegate with two variable signature
   * 
   * @param o Reference to observer
   * @param f Reference to delegate member function
   * @return delegate
   */  
  template<class O, typename F>
  auto inline delegate_2(O& o, F&& f) {
    auto b = std::bind(std::forward<F>(f), std::forward<O>(o), std::placeholders::_1, std::placeholders::_2);
    return delegate<decltype(b)>(std::forward<O>(o), b);
  }
}
#endif /* EVENT_HPP */

