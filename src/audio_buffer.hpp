#ifndef AUDIO_BUFFER_HPP
#define AUDIO_BUFFER_HPP

#include "audio_signal.hpp"
#include "audio_pass_basic.hpp"


namespace sfsw { namespace audio {

  /**
   * Buffer for holding the samples of the entire waveform
   * 
   * The buffer handles both mono and stereo interleaved waveforms.
   * 
   * mono, 1 channel:
   * One frame is one sample. On a filling operation, each sample  is duplicated 
   * in both the left and right channel of the window, thus making it stereo.
   * 
   * stereo, 2 channel:
   * One frame is two samples, interleaved (l,r,l,r). On a filling operation,
   * each sample is copied into the respective channel of the frame in the 
   * window.
   *
   *  
   * The filling behaviour can also be a linear fashion or looped
   * 
   * linear:
   * if a window is larger than what remains to be read, the operation will 
   * partially fill the window up to and including the last frame of the buffer, 
   * zero fill the remainder of the window and reset the buffer back to first frame.
   * 
   * looped:
   * if the window is larger than what remains to be read, the operation will 
   * fill the window up to and including the last frame of the buffer, then 
   * continue from the start of the buffer.
   * 
   * 
   */
class buffer {
public:
  enum iterator_behaviour {
    looped_iterator,  ///< iterator loops round to the beginning
    linear_iterator   ///< iterator zero signals at end of buffer
  };
  
  enum runtime_state {
    empty,      ///< Buffer has no data
    ready,       ///< Buffer in a reset and idle state
    active,     ///< Buffer is in an active state
    finished,   ///< Buffer is at the end (linear iterator)
  };

public:
  buffer(frame_count num_frames, channel_count num_channels = SFSW_NUM_CHANNELS);
  buffer(const buffer& orig) = delete;
  buffer(buffer&& orig);
  virtual ~buffer();
  
  /**
   * Get the working pointer to the data
   * @return 
   */
  f32sample_wrk data();
  
  /**
   * Copy date from specified buffer to internal buffer
   * @param data The buffer to copy from
   * @param num_frames The number of frames to copy
   */
  void copy_from(f32sample_wrk data, frame_count num_frames);
  
  /**
   * Get the number of samples in the buffer
   * @return 
   */
  sample_count num_samples();
  
  /**
   * Get the number of frames in the buffer
   * @return 
   */
  frame_count num_frames();
  
  /**
   * Get the number of channels across the buffer
   * @return int
   */
  channel_count num_channels();
  
  /**
   * Set the behaviour of the iterator
   * 
   * @param behaviour
   */
  void set_behaviour(iterator_behaviour behaviour);
  
  /**
   * Copy a window of the buffer into the signal
   * 
   * @param signal Reference to the signal
   */
  void copy_window(signal& signal);
  
  /**
   * Perform the pass operation
   * @param signal Reference to signal
   */
  void operator()(signal& signal);
  
  /**
   * Notify the buffer that it's data is now ready
   */
  void notify_data_ready();
  
  /**
   * Notify the buffer that it's data is now empty
   */
  void notify_data_empty();
  
  /**
   * Get the current runtime state of buffer
   * @return runtime_state The current state
   */
  runtime_state state();
  
  /**
   * Check if data is ready to be read
   * @return bool
   */
  bool data_ready();
  
  /**
   * Reset the read head
   */
  void reset();
  
  /**
   * Reset the read head and set runtime state
   * @param state The state to change to
   */
  void reset(runtime_state state);
  
  /**
   * Seek read head forward a number of frames
   * 
   * Linear:
   *  if goes beyond scope, will stop at last frame and enter finished state
   * 
   * Looped:
   *  if goes beyond scope, will loop from the beginning
   * 
   * @param num_frames The number of frames to seek forward
   */
  void seek(frame_count num_frames);
  
  /**
   * Rewind read head back a number of frames
   * 
   * Linear:
   *  if goes beyond 0, will stop at first frame and enter ready state
   * 
   * Looped:
   *  if goes beyond 0, will loop from the end 
   * 
   * @param num_frames The number of frames to seek back
   */
  void rewind(frame_count num_frames);
  
  /**
   * Get current position of read head in frames
   */
  frame_count position();
  
private:
  iterator_behaviour _behaviour;    ///< The behaviour of the iterator
  runtime_state _runtime_state;     ///< The current state of the buffer

  frame_count _frame_count;         ///< Number of frames in the buffer
  channel_count _channel_count;     ///< Number of channels in the buffer
  sample_count _sample_count;       ///< Total number of samples in the buffer
  f32sample_uptr _buffer;           ///< The buffer holding all the samples
  frame_count _read_head;          ///< The read head pointer to current frame

  /**
   * Fill channels with samples from a stereo buffer
   * 
   * This will move the read-head forward by size
   * 
   * @param signal The signal to fill
   * @param size Number of frames to fill
   */
  inline void fill_stero(signal& signal, frame_count frames) {
    signal.pass(pass::interleaved_fill(read_position(two_channel), frames));
    sync_read_head(frames, attr::position::forward);
  }
  
  /**
   * Fill channels with samples from a mono buffer
   * 
   * This will move the read-head forward by size
   * 
   * @param signal The signal to fill
   * @param size Number of frames to fill
   */
  inline void fill_mono(signal& signal, frame_count frames) {
    signal.pass(pass::mono_fill(read_position(one_channel), frames));
    sync_read_head(frames, attr::position::forward);
  }
  
  /**
   * Get the window size to read from the buffer
   * @return 
   */
  inline frame_count window_size(signal& signal) {
    auto total = signal.num_available();
    
    // Check if we're going over
    auto rem = _frame_count - _read_head;
    if(rem < total) { total = rem; }
    
    return total;
  }
  
  inline frame_count window_diff(frame_count window_size, signal& signal) {
    return signal.num_available() - window_size;
  }
  
  /**
   * Fill interleaved samples from stereo buffer into signal
   * 
   * @param signal
   */
  inline void stereo_fill(signal& signal) {
    auto size = window_size(signal);
    auto diff = window_diff(size, signal);
    
    fill_stero(signal, size);
    
    if(diff > 0) {
      // not enough frames left in buffer to fill out signals
      switch(_behaviour) {
        case iterator_behaviour::linear_iterator:
          // We get to the end and return to ready
          signal.pass(pass::zero_fill(diff));
          reset(runtime_state::ready);
          break;
          
        case iterator_behaviour::looped_iterator:
          // We are looping back to the beginning
          reset();
          fill_stero(signal, diff);
      }
    }
  }
  
  /**
   * Fill samples from mono buffer into signal
   * @param signal
   */
  inline void mono_fill(signal& signal) {
   auto size = window_size(signal);
    auto diff = window_diff(size, signal);
   
    fill_mono(signal, size);

    if(diff > 0) {
      // not enough frames left in buffer to fill out signals
      switch(_behaviour) {
        case iterator_behaviour::linear_iterator:
          // We get to the end and return to ready
          signal.pass(pass::zero_fill(diff));
          reset(runtime_state::ready);
          break;
          
        case iterator_behaviour::looped_iterator:
          // We are looping back to the beginning
          reset();
          fill_mono(signal, diff);
      }
    }
  }
  
   /**
   * Fill channels with samples from a mono buffer
   * 
   * This will move the read-head forward by size
   * 
   * @param left Working pointer to left channel
   * @param right Working pointer to right channel
   * @param size Number of frames to fill
   */
  void inline frame_fill_mono(f32sample_wrk left, f32sample_wrk right, frame_count size, frame_count offset) {
    auto rh = _read_head;
    for(auto frame = 0u; frame < size; frame++) {
      auto pos = offset+frame;
      left[pos] = _buffer[rh + frame];
      right[pos] = _buffer[rh + frame];
    }
    _read_head += size;
  }
  
  
  /**
   * Set the runtime state of the buffer
   * @param rstate
   */
  inline void set_state(runtime_state rstate) {
    _runtime_state = rstate;
  }
  
  inline f32sample_wrk read_position( channel_count num_channels) {
    return _buffer.get() + ( _read_head * num_channels );
  }
  
  /**
   * Sync the read head to a frame position in the buffer
   * 
   * @param frame The number of frames to sync to
   */
  void sync_read_head(frame_count frame, attr::position direction);
};

using buffer_uptr = std::unique_ptr<buffer>;
} }
#endif /* AUDIO_BUFFER_HPP */

