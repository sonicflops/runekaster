#ifndef THROWABLES_HPP
#define THROWABLES_HPP

namespace sfsw {
    class resource_not_found : public std::exception {
    public:
        resource_not_found() { }
        virtual const char* what() const throw() { 
            return "Resource was not found";
        }
    };
    
    class format_error : public std::exception {
    public:
        format_error() { }
        virtual const char* what() const throw() { 
            return "Format error or unsupported";
        }
    };
}

#endif /* THROWABLES_HPP */

