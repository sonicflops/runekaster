#ifndef LABELED_HPP
#define LABELED_HPP

namespace sfsw {
  

class labeled {
public:
  labeled(std::string label)
    : _label(label)
  { }
    
  labeled(const labeled& orig)
    :_label(orig._label)
  { }
  
  labeled(const labeled&& orig)
    :_label(orig._label)
  { }
    
  std::string label() {
    return _label;
  }
  
  void relabel(std::string label) {
    _label = label;
  }
  
protected:
  std::string _label;
};

}
#endif /* LABELED_HPP */

