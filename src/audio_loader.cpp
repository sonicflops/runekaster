#include <sndfile.h>
#include "audio_loader.hpp"


sfsw::audio::buffer_uptr sfsw::audio::load_buffer(std::string path) {
  SF_INFO info;
  info.format = 0;
  auto fp = sf_open(path.c_str(), SFM_READ, &info);
  
  
  if(fp == nullptr) {
    throw sfsw::resource_not_found();
  }

  auto nc = info.channels;
  auto nf = info.frames;
  
  auto b = new buffer(info.frames, info.channels);
  if(sf_readf_float(fp, b->data() , nf) == 0) {
    throw sfsw::format_error();
  }
  sf_close(fp);
  b->notify_data_ready();
  return buffer_uptr(b);
}
