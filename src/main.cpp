#include <stdlib.h>

#include "jack_client.hpp"
#include "midi_event.hpp"
#include "audio_layer.hpp"
#include "interface_container.hpp"
#include "workspace.hpp"
 
using namespace sfsw;

static midi::event_bus bus;

static int jprocess(jack_nframes_t nframes, void* data) {
  auto& ifc = *reinterpret_cast<sfsw::interface_container*>(data);
  for(auto& port : ifc.system_jack_client().ports().midi_inputs()) {
    jack_midi_event_t event;
    auto buf = jack_port_get_buffer(port.second, nframes);
    auto eindex = 0u;
    auto ecount = jack_midi_get_event_count(buf);
    

    for(eindex = 0; eindex < ecount; eindex++) {
      jack_midi_event_get(&event, buf, eindex);
      sfsw::midi::message m(event.buffer);
      bus.handle(m);
    }
//    jack_get_ports();
    
//    jack_midi_event_get(&event, buf,0);
  }
  return 0;
}

static void jshutdown(void*) {
  std::cout << "Jack shutdown\n";
}

int main(int /*argc*/, char** /*argv[]*/) {
  
  sfsw::workspace workspace;
  
  workspace.system_jack_client().set_process_data(static_cast<void*>(&workspace));
  
  if(!workspace.system_jack_client().open(jprocess, jshutdown)) {
    exit(EXIT_FAILURE);
  }
  
  std::cout << "Samplerate: " << workspace.system_jack_client().samplerate() << "\n";
  
  if(!workspace.system_jack_client().activate()) {
    std::cerr << "Failed to activate client\n";
  }
  
  workspace.add_layer("foobar");
 
  
//  client.register_audio_output("output_loop_l");
//  client.register_audio_output("output_loop_r");
//  client.register_midi_input("controller");
  
  sfsw::audio::layer subject("subject"), observer("observer");
  
  //subject.subscribe(subject.event_sync_start, delegate_1(&observer,&audio::layer::on_sync_start));
  

  std::this_thread::sleep_until(std::chrono::time_point<std::chrono::system_clock>::max());
  exit(EXIT_SUCCESS);
}
