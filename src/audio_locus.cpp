#include <vector>

#include "audio_locus.hpp"
using namespace sfsw::audio;

locus::locus(std::string label)
  : labeled(label)
{ }

locus::locus(std::string label, stream_info info)
  : labeled(label)
  , _stream(info)
{ }

locus::locus(const locus& orig)
  : labeled(orig)
{ }

locus::~locus() {
}

void locus::add_layer(std::string label) {
  _layers.push_back(layer(label,  _stream.period));
}

bool locus::remove_layer(std::string label) {
  bool changed = false;
  
  layer_container tmp;
  
  for(auto it = _layers.begin(); it != _layers.end(); it++) {
    if( (*it).label() == label ) {
      changed = true;
    } else {
      tmp.push_back(std::move(*it));
    }
  }
  _layers = std::move(tmp);
  return changed;
}

bool locus::relabel_layer(std::string old_label, std::string new_label) {
  if(label_exist(new_label)){ return false; }
  for(auto& layer : _layers) {
    if( layer.label() == old_label ) {
      layer.relabel(new_label);
      return true;
    }
  }
  return false;
}


locus::layer_container& locus::endpoints() {
  return _layers;
}