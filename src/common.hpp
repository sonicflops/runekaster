#ifndef COMMON_HPP
#define COMMON_HPP

#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <thread>
#include <chrono>
#include <functional>
#include <mutex>

#include <jack/jack.h>
#include <jack/midiport.h>

#include "uid.hpp"
#include "audio_common.hpp"
#include "throwables.hpp"

#endif /* COMMON_HPP */

