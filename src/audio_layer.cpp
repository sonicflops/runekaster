#include <iostream>
#include "audio_layer.hpp"
#include "audio_loader.hpp"

using namespace sfsw::audio;
using namespace std::placeholders;
layer::layer(std::string label)
  : labeled(label)
  , _buffer(new buffer(0,2))
  , _signal(SFSW_PERIOD_SIZE)
  , _state(runtime_state::empty)
  , _behaviour(runtime_behaviour::free_loop)
  , _prefill(0)
  , _uid(sfsw::uid::generate())
{ }

layer::layer(std::string label, frame_count period_size)
  : labeled(label)
  , _buffer(new buffer(0,2))
  , _signal(period_size)
  , _state(runtime_state::empty)
  , _behaviour(runtime_behaviour::free_loop)
  , _prefill(0)
  , _uid(sfsw::uid::generate())
{ }

layer::layer(layer&& orig)
  : labeled(orig)
  , _buffer(std::move(orig._buffer))
  , _signal(std::move(_signal))
  , _state(orig._state)
  , _behaviour(orig._behaviour)
  , _prefill(orig._prefill)
  , _uid(orig._uid)
{ }

layer::~layer() {
}


inline void layer::notify_sync_start() {
  event_sync_start.notify();
}

void layer::on_sync_start() {
  if(state() == runtime_state::cued) {
    std::cout << "Switcing to active" << std::endl;
    set_state(runtime_state::active);
  }
}

void layer::on_sync_bar(frame_count diff) { 
  _prefill = diff;
}

inline void layer::notify_sync_bar() {
  auto frame_diff = 0;
  event_sync_bar.notify(frame_diff);
}

std::string layer::endpoint_name() {
  return _label;
}

const endpoint& layer::endpoint_reference() {
  return _endpoint;
}

void layer::load_audio(std::string path) {
  _buffer = audio::load_buffer(path);
  if(_buffer->data_ready()) {
    set_state(runtime_state::ready);
  }
}

void layer::load_audio(f32sample_wrk data, frame_count frames, channel_count channels) {
  auto tmp = new buffer(frames, channels);  
  _buffer.reset(tmp);
  _buffer->copy_from(data, frames);
  set_state(runtime_state::ready);
}

layer::runtime_state layer::state() {
  return _state;
}

layer::runtime_behaviour layer::behaviour() {
  return _behaviour;
}

void layer::set_bahaviour(runtime_behaviour behaviour) {
  _behaviour = behaviour;
}

void layer::controller_start() {
  
  switch(behaviour()) {
    
    case runtime_behaviour::sync_loop:
    case runtime_behaviour::sync_linear:
      buf().reset(buffer::runtime_state::active);
      set_state(runtime_state::cued);
      break;

    case runtime_behaviour::free_linear:
    case runtime_behaviour::free_loop:
      buf().reset(buffer::runtime_state::active);
      set_state(runtime_state::active);
      notify_sync_start();
      break;
  }
}


void layer::controller_stop() {
  set_state(runtime_state::ready);
  buf().reset(buffer::ready);
}

/*
 * @todo handle bars and smaller increments
 */
bool layer::run_pipeline() {
    if(state() != runtime_state::active){ return false; }
    
    if(buf().state() == buffer::ready) {
      notify_sync_start(); // We have now started
    }
    
    _signal.reset();
    
    prefill(_signal);
    
    _signal.pass(buf());
    for(auto& f : _pipeline) {
      _signal.pass(f);
    }
    _signal.pass(_endpoint);
    
    resolve_state();
    return true;
}

layer::audio_pipeline& layer::pipeline() {
  return _pipeline;
}

void layer::add_pass(pass::signal_pass op) {
  _pipeline.push_back(op);
}

sfsw::uid::unique_id layer::uid() {
  return _uid;
}

