#ifndef AUDIO_COMMON_HPP
#define AUDIO_COMMON_HPP

#include <cstdint>

#define SFSW_PERIOD_SIZE 512  ///< Number of frames in each period
#define SFSW_NUM_CHANNELS 2   ///< Number of channels


namespace sfsw { namespace audio {
  
  using f32sample = float; ///< An PCM sample
  using f32sample_wrk = f32sample*; ///< Non-owning working pointer to sample data
  using f32sample_uptr = std::unique_ptr<f32sample[]>; ///< Unique_ptr of samples
  
  using frame_count = std::uint32_t;  ///< Type indicating a number of frames
  using sample_count = std::uint32_t; ///< Type indicating a number of samples
  using channel_count = std::uint32_t; ///< Type indicating a number of channels
  
  /**
   * Global types for channels
   * 
   * Convert to numeric index with strong_cast
   */
  enum class stereo_channel : int {
    left, ///< Left channel index
    right ///< Right channel index
  };
  
  /**
   * Convert a strongly typed enumerator to integer
   * @param v The type to convert
   * @return int
   */
  template<typename T>
  inline int strong_cast(const T v) {
    return static_cast<int>(v);
  }
  
  using signal_channel = f32sample*;  ///< Type representing a channel
  using channel_uptr = std::unique_ptr<f32sample[]>;  ///< Unique ptr of channel
  using channel_container = std::array<channel_uptr, SFSW_NUM_CHANNELS>; ///< A container for channels 
  
  enum {
    one_channel = 1, two_channel = 2,
  };
  
  /**
   * Information about a stream
   */
  struct stream_info {
    
    /**
     * Construct a stream info with defaulted parameters
     */
    stream_info() : period(SFSW_PERIOD_SIZE), channels(SFSW_NUM_CHANNELS) { }
    
    /**
     * Construct a stream info with specific parameters
     * @param p The period size in frames
     * @param c The number of channels
     */
    stream_info(frame_count p, channel_count c)
      : period(p), channels(c)
    { }
    frame_count period;
    channel_count channels;
  };
  
} }

#include "audio_attr.hpp"


#endif /* AUDIO_COMMON_HPP */

