#ifndef AUDIO_PASS_BASIC_HPP
#define AUDIO_PASS_BASIC_HPP

#include "audio_pass_common.hpp"


namespace sfsw { namespace audio { namespace pass {

  /**
   * Zero fill a signal with specified number of frames
   * 
   * This functor will not inspect the signal for frame ranges and assumes that
   * the numbers provided are valid
   * 
   * @param num_frames The number of frames to write to the signal
   */
  class zero_fill : public signal_operation {
  public:
    zero_fill(frame_count num_frames)
    : _frames(num_frames)
    { }
    
    void operator()(audio::signal& signal) override {
      auto left = signal.channel_synced(stereo_channel::left);
      auto right = signal.channel_synced(stereo_channel::right);
      
      for(auto frame = 0u; frame < _frames; frame++) {
        left[frame] = 0.0f; right[frame] = 0.0f;
      }
      
      signal.sync_write_head(_frames, attr::position::forward);
    }
    
    std::string name() override {
      return "basic.zerofill";
    };

  private:
    frame_count _frames;
  };
  
  /**
   * Fill a signal with specified number of frames from an interleaved sample source
   * 
   * This functor will not inspect the signal for frame ranges and assumes that
   * the numbers provided are valid
   * 
   * @param source The source of the interleaved data
   * @param num_frames The number of frames to write to the signal
   */
  class interleaved_fill : public signal_operation {
  public:
    interleaved_fill(f32sample_wrk source, frame_count num_frames)
      : _source(source), _frames(num_frames)
    { }
      
    void operator()(audio::signal& signal) override {
      auto left = signal.channel_synced(stereo_channel::left);
      auto right = signal.channel_synced(stereo_channel::right);
    
      for(auto frame = 0u; frame < _frames; frame++) {
        auto ls = frame * two_channel;
        auto rs = ls + 1;
        left[frame] = _source[ls];
        right[frame] = _source[rs];
      }
      
      signal.sync_write_head(_frames, attr::position::forward);
    }
    
    std::string name() override {
      return "basic.interleaved_fill";
    }

  private:
    f32sample_wrk _source;
    frame_count _frames;
  };
  
  /**
   * Fill a signal with specified number of frames from a mono sample source
   * 
   * This functor will not inspect the signal for frame ranges and assumes that
   * the numbers provided are valid
   * 
   * @param source The source of the interleaved data
   * @param num_frames The number of frames to write to the signal
   */ 
  class mono_fill : public signal_operation {
  public:
    mono_fill(f32sample_wrk source, frame_count num_frames)
    : _source(source), _frames(num_frames)
    { }
    
    void operator()(audio::signal& signal) override {
      auto left = signal.channel_synced(stereo_channel::left);
      auto right = signal.channel_synced(stereo_channel::right);
      
      for(auto frame = 0u; frame < _frames; frame++) {
        left[frame] = _source[frame];
        right[frame] = _source[frame];
      }
      
      signal.sync_write_head(_frames, attr::position::forward);
    }
    
    std::string name() override {
      return "basic.mono_fill";
    }

  private:
    f32sample_wrk _source;
    frame_count _frames;
  };
  
} } }

#endif /* AUDIO_PASS_BASIC_HPP */

