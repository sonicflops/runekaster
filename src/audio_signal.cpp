#include "audio_signal.hpp"
using namespace sfsw::audio;
signal::signal(frame_count period_size)
  : _period_size(period_size), _write_head(0), _available(period_size)
{
  for(auto i = 0u; i < SFSW_NUM_CHANNELS; i++) {
    _channels[i] = std::move(channel_uptr(new f32sample[period_size]));
  }
}

signal::signal(const signal&) {
  
}

signal::~signal() { 
  
}

f32sample_wrk signal::channel(stereo_channel index) {
  return _channels[strong_cast(index)].get();
}

f32sample_wrk signal::channel_synced(stereo_channel index) {
  return _channels[strong_cast(index)].get() + _write_head;
}


frame_count signal::num_total() {
  return _period_size;
}

frame_count signal::num_available() {
  return _available;
}

void signal::sync_write_head(frame_count frame, attr::position direction) {
  switch(direction) {
      case attr::position::forward:
        if(frame > _available) { frame = _available; }
        _write_head += frame;
        break;

      case attr::position::backward:
        if(_write_head < frame) { frame = _write_head; }
        _write_head -= frame;
        break;
      
      case attr::position::absolute:
        if( frame > _period_size - 1){ frame = _period_size - 1; }
        _write_head = frame;
        break;
    }
    
    _available = _period_size - _write_head;
}

void signal::reset() {
  _write_head = 0;
  _available = _period_size;
}



frame_count signal::position() {
  return _write_head;
}


