#ifndef WORKSPACE_HPP
#define WORKSPACE_HPP
#include "interface_container.hpp"
namespace sfsw {
/**
 * This represents a whole workspace, including grouping layers, locus
 * and is the keeper of the buses
 */
class workspace
  : public interface_container
{
public:
  enum sync_type {
    sync_start, sync_bar
  };
public:
  workspace();
  workspace(audio::stream_info info);
  workspace(const workspace& orig) = delete;
  virtual ~workspace();

  void init();
  // interface_container methods
   
  audio::locus& system_active_locus() override;
  jack::client& system_jack_client() override;
  midi::event_bus& system_midi_bus() override;
  
  /**
   * Add a locus to the workspace
   * 
   * @param label
   */
  void add_locus(std::string label) noexcept;
  
  /**
   * Add a layer to the current locus
   * 
   * @param label The name of the layer
   */
  void add_layer(std::string label) noexcept;
  
  /**
   * Remove a layer from the current locus
   * 
   * @param label The label of the layer to remove
   * @return True if found and removed
   */
  bool remove_layer(std::string label) noexcept;
  
  /**
   * Relabel a layer on the current locus
   * 
   * @param old_label The label of the layer to change
   * @param new_label The label to change to
   * @return True if found and removed
   */
  bool relabel_layer(std::string old_label, std::string new_label) noexcept;
  
  /**
   * Sync an observing layer with a subject layer
   * 
   * Here the subject is primary and observer syncs off the subject
   * 
   * @param subject The sync subject layer
   * @param observer The sync observing layer
   * @param type The type to sync
   * @return True if successfully synced
   */
  bool create_layer_sync(std::string subject, std::string observer, sync_type type) noexcept;
  
  /**
   * Drop a sync between a layer
   * 
   * This will drop all syncs between the subject and observer
   * 
   * @param subject The sync subject layer
   * @param observer The sync observer layer
   * @return True if successfully dropped
   */
  bool drop_layer_sync(std::string subject, std::string observer);
  
  /**
   * Find a layer in the current locus with given label
   * 
   * If a layer with the specified label is not found in the current locus
   * then the function will throw out_of_range exception
   * 
   * @param label The label of the layer to find
   * @throw out_of_range
   * @return A reference to the layer
   */
  audio::layer& layer_from_label(std::string label);
private:

  midi::event_bus _midi_bus;
  jack::client _jack_client;
  std::vector<audio::locus> _loci;
  audio::stream_info _stream;
  
  unsigned int _active_locus;
};

}
#endif /* WORKSPACE_HPP */

