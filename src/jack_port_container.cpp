#include "jack_port_container.hpp"
using namespace sfsw::jack;


port_container::port_container(jack_client_t* client)
  : _client(client)
{ }

port_container::port_container(const port_container& orig) 
  : _midi_inputs(orig._midi_inputs)
  , _audio_outputs(orig._audio_outputs)
  , _client(orig._client)
{ }

port_container::~port_container() {
}

void port_container::reset_client(jack_client_t* client) {
  _client = client;
}

// Midi handling

bool port_container::register_midi_input(std::string name) {
  return action_register_midi_input(name);
}

bool port_container::unregister_midi_input(std::string name) {
  return action_unregister_midi_input(name);
}

bool port_container::rename_midi_input(std::string old_name, std::string new_name) {
  return action_rename_midi_input(old_name, new_name);
}

jack_port_t* port_container::midi_input(std::string name) {
  auto it = _midi_inputs.find(name);
  if(it == _midi_inputs.end()){ return nullptr; }
  return it->second;
}

port_container::port_rack& port_container::midi_inputs() {
  return _midi_inputs;
}

// Audio handling =======

bool port_container::register_audio_outputs(std::string name) {
  return action_register_audio_output(name+"_l")
      && action_register_audio_output(name+"_r");
}


bool port_container::unregister_audio_outputs(std::string name) {
  return action_unregister_audio_ouput(name+"_l")
      && action_unregister_audio_ouput(name+"_r");
}

bool port_container::rename_audio_outputs(std::string old_name, std::string new_name) {
  return action_rename_audio_output(old_name+"_l", new_name+"_l")
      && action_rename_audio_output(old_name+"_r", new_name+"_r");
}


jack_port_t* port_container::audio_output(std::string name) {
  auto it = _audio_outputs.find(name);
  if(it == _audio_outputs.end()) { return nullptr; }
  return it->second;
}

jack_port_t* port_container::audio_output(std::string name, audio::stereo_channel index) {
  port_rack::iterator it;
  switch(index) {
    case audio::stereo_channel::left:
      it = _audio_outputs.find(name+"_l");
      break;
    case audio::stereo_channel::right:
      it = _audio_outputs.find(name+"_r");
      break;
  }
  
  if(it == _audio_outputs.end()) { return nullptr; }
  return it->second;
}

port_container::port_ptr port_container::jack_register(std::string name, const char* type, unsigned long flags, unsigned long buffer_size) {
  return jack_port_register(_client, name.c_str(), type, flags,buffer_size);
}

bool port_container::jack_rename(port_ptr port, std::string new_name) {
  if(jack_port_set_name(port, new_name.c_str()) != 0){ return false; }
  return true;
}

bool port_container::jack_unregister(port_ptr port) {
  if(jack_port_unregister(_client, port) != 0){ return false; }
  return true;
}

bool port_container::action_register_audio_output(std::string name) {
  if(_audio_outputs.find(name) != _audio_outputs.end()){ return false; }
  
  auto port = jack_register(name, JACK_DEFAULT_AUDIO_TYPE, JackPortIsOutput);
  if(port == nullptr) {
    std::cerr << "No more JACK ports available\n";
    return false;
  }
  
  _audio_outputs.insert(port_pair(name, port));
  return true; 
}

bool port_container::action_unregister_audio_ouput(std::string name) {
  auto it = _audio_outputs.find(name);
  if(it == _audio_outputs.end()){ return false; }
  
  if(!jack_unregister(it->second)){ return false; }
  
  _audio_outputs.erase(it);
  return true;
}

bool port_container::action_rename_audio_output(std::string old_name, std::string new_name) {
  if(old_name == new_name){ return false; }
  auto it = _audio_outputs.find(old_name);
  if(it == _audio_outputs.end()){ return false; }
  
  auto ptr = it->second;
  _audio_outputs.erase(it);
  
  if(!jack_rename(ptr, new_name)){  return false; }
  std::swap(_audio_outputs[new_name], ptr);
  
  return true;
}

bool port_container::action_register_midi_input(std::string name) {
  if(_midi_inputs.find(name) != _midi_inputs.end()){ return false; }
  auto port = jack_register(name, JACK_DEFAULT_MIDI_TYPE, JackPortIsInput);
   if(port == nullptr) {
    std::cerr << "No more JACK ports available\n";
    return false;
  }
  
  _midi_inputs.insert(port_pair(name, port));
  return true; 
}

bool port_container::action_unregister_midi_input(std::string name) {
  auto it = _midi_inputs.find(name);
  if(it == _midi_inputs.end()){ return false; }
  
  if(!jack_unregister(it->second)){ return false; }
  _midi_inputs.erase(it);
  return true;
}

bool port_container::action_rename_midi_input(std::string old_name, std::string new_name) {
  if(old_name == new_name){ return false; }
  
  auto it = _midi_inputs.find(old_name);
  if(it == _midi_inputs.end()){ return false; }
  
  auto ptr = it->second;
  _midi_inputs.erase(it);
  
  if(!jack_rename(ptr, new_name)){ return false; }
  std::swap(_midi_inputs[new_name], ptr);
  
  return true;
}




