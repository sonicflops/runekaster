#ifndef JACK_PORT_MANAGER_HPP
#define JACK_PORT_MANAGER_HPP
#include "common.hpp"
namespace sfsw { namespace jack {
  
  
  
  class port_container {
  public:
    typedef jack_port_t jack_port;
    typedef jack_port* port_ptr;
    
  private:
    using port_pair = std::pair<std::string, port_ptr>;
    using port_rack = std::map<std::string, port_ptr>;
    
  public:
    port_container(jack_client_t* client);
    port_container(const port_container& orig);
    virtual ~port_container();
    
  /**
   * Reset the pointer to the client with new pointer
   * 
   * @param client Pointer to the new client
   */
  void reset_client(jack_client_t* client);

    
  /**
   * Register a midi input with given name
   * 
   * @param name
   * @return True if successfully changed
   */
  bool register_midi_input(std::string name);
  
  
    /**
   * Unregister a midi port
   * 
   * @param name
   * @return True if successfully unregistered
   */
  bool unregister_midi_input(std::string name);
  
  
  
   /**
   * Rename a midi input
   * 
   * @param old_name The name of the midi input to change
   * @param new_name The new name for the given input
   * @return True if successfully changed
   */
  bool rename_midi_input(std::string old_name, std::string new_name);

  
  /**
   * Get the pointer to the jack port for a midi input
   * @param name The name of the port
   * @return port_ptr | nullptr
   */
  port_ptr midi_input(std::string name);
  
  
  /**
   * Get a reference to all the rack of midi input ports
   * 
   * @return jport_rack&
   */
  port_rack& midi_inputs();
  
  
  
  // Audio member functions
  
  /**
   * Register two ports (name_l, name_r) to create a stereo port collection
   * 
   * @param name Base name
   * @return bool True if added successfully
   */
  bool register_audio_outputs(std::string name);
  
   /**
   * Remove a stereo port collection with given base name
   * 
   * @param name The base name of the ports
   * @return True if removed successfully
   */
  bool unregister_audio_outputs(std::string name);

  /**
   * Change the name of a stereo port collection
   * 
   * @param old_name The old base name
   * @param new_name The new base name
   * @return True if changed successfully
   */
  bool rename_audio_outputs(std::string old_name, std::string new_name);
  
  /**
   * Get the port with a given absolute name
   * 
   * If the port does not exist then a nullptr is returned
   * 
   * @param name The absolute name of the port
   * @return jack_port_t | nullptr
   */
  jack_port_t* audio_output(std::string name);
  
  /**
   * Get the specified channel for a stereo port collection
   * 
   * The port will be resolved into `name_l` or `name_r`.
   * If the port does not exist then a nullptr is returned
   * 
   * @param name The name of the port
   * @param index The channel to retrieve
   * @return jack_port_t | nullptr
   */
  port_ptr audio_output(std::string name, audio::stereo_channel index);

  private:

    /**
     * Register a port with jack
     * 
     * This is an internal function used by the public methods
     * 
     * @param name The name of the port
     * @param type The type of the port
     * @param flags The flags attached to the port
     * 
     * @return port_ptr | nullptr
     */
    port_ptr jack_register(std::string name, const char* type, unsigned long flags, unsigned long buffer_size = 0);
    
    /**
     * Unregister a port with jack
     * 
     * This is an internal function used by public methods
     * 
     * @param port The pointer to the port
     * @return True if successfully unregistered
     */
    bool jack_unregister(port_ptr port);
    
    /**
     * Rename a jack port
     * 
     * This is an internal function used by the public methods
     * 
     * @param port Pointer to the port
     * @param new_name The new to change to
     * @return True if successful
     */
    bool jack_rename(port_ptr port, std::string new_name);
    
    /**
     * Register an audio output here and in jack
     * 
     * @param name The name of the output
     * @return True if action is successful
     */
    bool action_register_audio_output(std::string name);
    
    
    /**
     * Unregister an audio output here and in jack
     * 
     * @param name The name of the port to unregister
     * @return True if unregistered successfully
     */
    bool action_unregister_audio_ouput(std::string name);
    
    /**
     * Rename an audio output here and in jack
     * 
     * @param old_name The name of the port to rename
     * @param new_name The name to change to
     * @return True if renamed successfully
     */
    bool action_rename_audio_output(std::string old_name, std::string new_name);
    
    
    /**
     * Register a midi port here and in jack
     * 
     * @param name The name of the midi input
     * @return True if successfully registered
     */
    bool action_register_midi_input(std::string name);
    
    /**
     * Unregister a midi port here and in jack
     * 
     * @param name The name of the port to unregister
     * @return True if successfully unregistered
     */
    bool action_unregister_midi_input(std::string name);
    
    /**
     * Rename a midi port here and in jack
     * 
     * @param old_name Name of the port to rename
     * @param new_name Name to change port to
     * @return True if successfully renamed
     */
    bool action_rename_midi_input(std::string old_name, std::string new_name);
    jack_client_t* _client;
    port_rack _audio_outputs;
    port_rack _midi_inputs;
  };

} }
#endif /* JACK_PORT_MANAGER_HPP */

