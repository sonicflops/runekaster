#include "uid.hpp"

sfsw::uid::unique_id sfsw::uid::generate() {
    static std::atomic<unsigned int> track(0);
    return track.fetch_add(1, std::memory_order_seq_cst);
}