#ifndef MIDI_EVENT_HPP
#define MIDI_EVENT_HPP
#include "midi_message.hpp"

namespace sfsw { namespace midi {
  
  using event_id = unsigned int;

  /**
   * Base class for anything that handles MIDI events
   */
  class event_handler {
  public:
    virtual ~event_handler() { }
    
    void event_note_on(message m, event_id id) { }
    void event_note_off(message m, event_id id) { }
    void event_control_change(message m, event_id id) { }
    void event_channel_pressure(message m, event_id id) { }
    void event_pitch_bend(message m, event_id id) { }
    void event_poly_key_pressure(message m, event_id id) { }
    void event_program_change(message m, event_id id) { }
  };

  using event_callback = std::function<void(const message)>;
  
  /**
   * Event bus for sending midi messages to the correct end points
   */
  class event_bus {
  public:
    using event_key = std::pair<status_code,data_b1>;
    using event_pair = std::pair<event_key, event_callback>;
    using channel_list = std::map<event_key, event_callback>;
    
    /**
     * Register a handler for a particular message
     * 
     * @param status The status_code to listen for
     * @param b1 The first data byte to listen for (e.g. key,controller)
     * @param cb The callback to handle the message
     */
    void register_handler(status_code status, data_b1 b1, event_callback cb) {
      auto key = event_key(status,b1);
      auto it = _bus.find(key);
  
      if(it != _bus.end()) {
        _bus.erase(it); // we are going to overwrite handler
      }

      _bus.insert(event_pair(key, cb));
    }

    /**
     * Pass a message into the bus
     * 
     * @param m The message to send to any handlers
     */
    void handle(const message m) {
      auto it = _bus.find(event_key(m.status(),m.key()));
      if(it == _bus.end()){ return; }
      auto cb = it->second;
      
      cb(m);
    }
    
    /**
     * Assign a handler based on specified message
     * 
     * @param m The message used as reference
     * @param cb The callback to handle the message
     */
    void learn(message m, event_callback cb) {
      register_handler(m.status(), m.key(), cb);
    }
     
  private:
    channel_list _bus; ///< List of any handlers
  };
  
} }
#endif /* MIDI_EVENT_HPP */

