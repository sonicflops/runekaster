#ifndef AUDIO_PASS_COMMON_HPP
#define AUDIO_PASS_COMMON_HPP

#include "audio_signal.hpp"


namespace sfsw { namespace audio { namespace pass {
  
  
  using signal_pass = std::function<void(audio::signal&)>;
  
  /**
   * Convert a functor to a signal_pass
   * @param f The functor to convert
   * @return signal_pass
   */
  template<class F>
  inline signal_pass to_signal(F&& f) {
    signal_pass(std::forward<F>(f));
  }
  
  /**
   * Base class for a signal pass functor
   */
  class signal_operation {
  public:
    /**
     * Perform the operation on the signal
     * 
     * @param signal Signal to be operated on
     */
    virtual void operator()(audio::signal& signal) = 0;
    
    /**
     * Get the name of the pass
     * 
     * @return std::string Name of the pass
     */
    virtual std::string name() = 0;
  };

} } }

#endif /* AUDIO_PASS_COMMON_HPP */

