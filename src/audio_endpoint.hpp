#ifndef AUDIO_ENDPOINT_HPP
#define AUDIO_ENDPOINT_HPP
#include "audio_signal.hpp"

namespace sfsw { namespace audio {

class endpoint {
public:
  endpoint();
  endpoint(const endpoint& orig);
  virtual ~endpoint();
  
  void operator()(signal& signal);
  
  const f32sample_wrk channel(stereo_channel index) const;
  
private:
  std::array<f32sample_wrk,2> _channel;
};

class endpoint_container {
public:
  virtual std::string endpoint_name() = 0;
  virtual const endpoint& endpoint_reference() = 0;
};

} }

#endif /* AUDIO_ENDPOINT_HPP */

