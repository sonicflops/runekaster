cmake_minimum_required (VERSION 2.8.11)
project (SideWinder)
set(CMAKE_CXX_STANDARD 14)

set(COMMON_LIBS PUBLIC jack pthread sndfile)

# Include the swobj lib directory
add_subdirectory(src)

# Include tests directory
add_subdirectory(tests)

# Build bin in this directory
set(BIN_SRC ${CMAKE_CURRENT_SOURCE_DIR}/src/main.cpp )
add_executable (runekaster ${BIN_SRC})
target_include_directories( runekaster PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/src/)
target_link_libraries(runekaster PUBLIC swobj ${COMMON_LIBS})



