# RuneKaster

(Beginning of) audio looping software built to specific requirements, for Linux

## Dependencies

### Library Dependencies

**Jack**  
- ubuntu: `apt-get install libjack-dev`
- website: http://www.jackaudio.org/

**SndFile**  
- ubuntu: `apt-get install libsndfile1-dev`
- website: http://www.mega-nerd.com/libsndfile/

### Build Dependencies

**CMake**  
- ubuntu: `apt-get install cmake`

**C++14**  
- ubuntu: `apt-get install g++`
- ubuntu: `apt-get install clang`

### License

GPL v3.0